package com.jecc.pos.shipmentservice;

import org.axonframework.springboot.autoconfig.JdbcAutoConfiguration;
import org.axonframework.springboot.autoconfig.JpaAutoConfiguration;
import org.axonframework.springboot.autoconfig.JpaEventStoreAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
        scanBasePackages = {
                "com.jecc.pos.config",
                "com.jecc.pos.entities",
                "com.jecc.pos.commons",
                "com.jecc.pos.shipmentservice"
        },
        exclude = {
                JpaEventStoreAutoConfiguration.class,
                JpaAutoConfiguration.class,
                JdbcAutoConfiguration.class
        }
)
@EntityScan("com.jecc.pos.entities")
@EnableJpaRepositories("com.jecc.pos.shipmentservice.repository")
public class PosShipmentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PosShipmentServiceApplication.class, args);
    }

}
