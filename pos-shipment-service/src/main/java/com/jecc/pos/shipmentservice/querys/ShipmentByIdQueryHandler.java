package com.jecc.pos.shipmentservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.shipment.ShipmentByIdQueryProjection;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.entities.ShipmentEntity;
import com.jecc.pos.shipmentservice.repository.IShipmentRepository;
import com.jecc.pos.types.ShipmentDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ShipmentByIdQueryHandler implements Command.Handler<ShipmentByIdQueryProjection, ApiResponse<ShipmentDTO>> {

    private final IShipmentRepository ShipmentRepository;

    @Autowired
    public ShipmentByIdQueryHandler(IShipmentRepository ShipmentRepository) {
        this.ShipmentRepository = ShipmentRepository;
    }

    @Override
    public ApiResponse<ShipmentDTO> handle(ShipmentByIdQueryProjection ShipmentByIdQueryProjection) {
        Optional<ShipmentEntity> optionalShipmentEntity = this.ShipmentRepository.findByShipmentIdAndStatus(
                ShipmentByIdQueryProjection.getShipmentId(), ConstantsLibrary.STATUS_CREATED);
        optionalShipmentEntity.orElseThrow(() ->
                new CustomErrorException(
                        HttpStatus.NOT_FOUND,
                        ErrorsConstants.SHIPMENT_NOT_FOUND_CODE,
                        new String[]{ErrorsConstants.SHIPMENT_NOT_FOUND_MESSAGE},
                        ErrorsConstants.SHIPMENT_NOT_FOUND_CODE));
        return new ApiResponse.Builder<ShipmentDTO>()
                .builSuccesful(optionalShipmentEntity.get().getDTO())
                .build();
    }
}
