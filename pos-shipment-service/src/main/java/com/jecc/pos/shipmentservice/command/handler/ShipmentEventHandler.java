package com.jecc.pos.shipmentservice.command.handler;

import com.jecc.pos.commons.events.shipment.CancelShipmentEvent;
import com.jecc.pos.commons.events.shipment.CreateShipmentEvent;
import com.jecc.pos.entities.ShipmentEntity;
import com.jecc.pos.shipmentservice.repository.IShipmentRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class ShipmentEventHandler {

    private final IShipmentRepository shipmentRepository;

    @Autowired
    public ShipmentEventHandler(IShipmentRepository shipmentRepository) {
        this.shipmentRepository = shipmentRepository;
    }

    @EventHandler
    public void on(CreateShipmentEvent createShipmentEvent) {
        log.info("Handling createShipmentEvent start...");
        ShipmentEntity shipmentEntity = new ShipmentEntity();
        BeanUtils.copyProperties(createShipmentEvent, shipmentEntity);
        this.shipmentRepository.save(shipmentEntity);
        log.info("Handling createShipmentEvent end...");
    }

    @EventHandler
    public void on(CancelShipmentEvent cancelShipmentEvent) {
        log.info("Handling cancelShipmentEvent start...");
        Optional<ShipmentEntity> optionalShipmentEntity = this.shipmentRepository.findByShipmentIdAndStatus(cancelShipmentEvent.getShipmentId(),
                ConstantsLibrary.STATUS_CREATED);
        if (optionalShipmentEntity.isPresent()) {
            ShipmentEntity shipmentEntity = optionalShipmentEntity.get();
            shipmentEntity.setShipmentStatus(cancelShipmentEvent.getShipmentStatus());
            shipmentEntity.setUpdateAt(cancelShipmentEvent.getUpdateAt());
            shipmentEntity.setUserUpdate(cancelShipmentEvent.getUserUpdate());
            this.shipmentRepository.save(shipmentEntity);
        }
        log.info("Handling cancelShipmentEvent end...");
    }


}
