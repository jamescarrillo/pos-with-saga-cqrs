package com.jecc.pos.shipmentservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.shipment.ShipmentPaginationQueryProjection;
import com.jecc.pos.entities.ShipmentEntity;
import com.jecc.pos.shipmentservice.repository.IShipmentRepository;
import com.jecc.pos.types.ShipmentDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Log4j2
public class ShipmentPaginationQueryHandler implements Command.Handler<ShipmentPaginationQueryProjection, ApiResponse<Pagination<ShipmentDTO>>> {

    private final IShipmentRepository shipmentRepository;

    @Autowired
    public ShipmentPaginationQueryHandler(IShipmentRepository shipmentRepository) {
        this.shipmentRepository = shipmentRepository;
    }

    @Cacheable(value = "value", keyGenerator = "customKeyGenerator", cacheManager = "customCacheManager")
    @Override
    public ApiResponse<Pagination<ShipmentDTO>> handle(ShipmentPaginationQueryProjection shipmentPaginationQueryProjection) {
        log.info("ShipmentPaginationQueryHandler init");
        Pagination<ShipmentDTO> pagination = new Pagination<>();
        Pageable pageable = PageRequest.of(shipmentPaginationQueryProjection.getPage(), shipmentPaginationQueryProjection.getSize());
        pagination.setCountFilter(
                this.shipmentRepository.findCountEntities(ConstantsLibrary.STATUS_CREATED,
                        shipmentPaginationQueryProjection.getFilter())
        );
        pagination.setTotalPages(pagination.processAndGetTotalPages(shipmentPaginationQueryProjection.getSize()));
        if (pagination.getCountFilter() > 0) {
            Optional<List<ShipmentEntity>> optionalShipmentEntities = this.shipmentRepository.findEntities(
                    ConstantsLibrary.STATUS_CREATED,
                    shipmentPaginationQueryProjection.getFilter(),
                    pageable);
            pagination.setList(optionalShipmentEntities.orElse(new ArrayList<>()).stream().map(ShipmentEntity::getDTO).collect(Collectors.toList()));
        }
        log.info("ShipmentPaginationQueryHandler fin");
        return new ApiResponse.Builder<Pagination<ShipmentDTO>>()
                .builSuccesful(pagination)
                .build();
    }

}
