package com.jecc.pos.shipmentservice.command.aggregate;

import com.jecc.pos.commons.commands.shipment.cancel.CancelShipmentCommand;
import com.jecc.pos.commons.commands.shipment.create.CreateShipmentCommand;
import com.jecc.pos.commons.events.shipment.CancelShipmentEvent;
import com.jecc.pos.commons.events.shipment.CreateShipmentEvent;
import com.jecc.pos.types.ShipmentDTO;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Aggregate
@Slf4j
public class ShipmentAggregate extends ShipmentDTO {

    @AggregateIdentifier
    private String shipmentId;

    public ShipmentAggregate() {
    }

    @CommandHandler
    public ShipmentAggregate(CreateShipmentCommand createShipmentCommand,
                             @Autowired ShipmentAggregateValidator shipmentAggregateValidator) {
        log.info("createShipmentCommand received.");
        shipmentAggregateValidator.create(createShipmentCommand);
        CreateShipmentEvent createShipmentEvent = new CreateShipmentEvent();
        BeanUtils.copyProperties(createShipmentCommand, createShipmentEvent);
        AggregateLifecycle.apply(createShipmentEvent);
    }

    @CommandHandler
    public void on(CancelShipmentCommand cancelShipmentCommand,
                   @Autowired ShipmentAggregateValidator shipmentAggregateValidator) {
        log.info("cancelShipmentCommand received.");
        shipmentAggregateValidator.cancel(cancelShipmentCommand);
        CancelShipmentEvent cancelShipmentEvent = new CancelShipmentEvent();
        BeanUtils.copyProperties(cancelShipmentCommand, cancelShipmentCommand);
        AggregateLifecycle.apply(cancelShipmentEvent);
    }

    @EventSourcingHandler
    public void on(CreateShipmentEvent createShipmentEvent) {
        log.info("createShipmentEvent occurred.");
        this.shipmentId = createShipmentEvent.getShipmentId();
        this.setCode(createShipmentEvent.getCode());
        this.setShipmentStatus(createShipmentEvent.getShipmentStatus());
        this.setOrderId(createShipmentEvent.getOrderId());
        this.setStatus(createShipmentEvent.getStatus());
        this.setCreateAt(createShipmentEvent.getCreateAt());
        this.setUpdateAt(createShipmentEvent.getUpdateAt());
        this.setDeleteAt(createShipmentEvent.getDeleteAt());
        this.setUserCreate(createShipmentEvent.getUserCreate());
        this.setUserUpdate(createShipmentEvent.getUserUpdate());
        this.setUserDelete(createShipmentEvent.getUserDelete());
    }

    @EventSourcingHandler
    public void on(CancelShipmentEvent cancelShipmentEvent) {
        log.info("cancelShipmentEvent occurred.");
        this.shipmentId = cancelShipmentEvent.getShipmentId();
        this.setShipmentStatus(cancelShipmentEvent.getShipmentStatus());
        this.setUpdateAt(cancelShipmentEvent.getUpdateAt());
        this.setUserUpdate(cancelShipmentEvent.getUserUpdate());
    }

}
