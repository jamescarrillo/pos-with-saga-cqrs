package com.jecc.pos.shipmentservice.command.aggregate;

import com.jecc.pos.commons.commands.shipment.cancel.CancelShipmentCommand;
import com.jecc.pos.commons.commands.shipment.create.CreateShipmentCommand;
import com.jecc.pos.entities.ShipmentEntity;
import com.jecc.pos.shipmentservice.repository.IShipmentRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class ShipmentAggregateValidator {

    private final IShipmentRepository shipmentRepository;

    @Autowired
    public ShipmentAggregateValidator(IShipmentRepository shipmentRepository) {
        this.shipmentRepository = shipmentRepository;
    }

    public void create(CreateShipmentCommand createShipmentCommand) {
        log.info("Validator createShipmentCommand init");
        Optional<ShipmentEntity> optionalShipmentEntity =
                this.shipmentRepository.findByOrderId(
                        createShipmentCommand.getOrderId(),
                        ConstantsLibrary.STATUS_CREATED);
        if (optionalShipmentEntity.isPresent()) {
            throw new CommandExecutionException(ErrorsConstants.SHIPMENT_ALREADY_EXIST_CODE, null, new String[]{ErrorsConstants.SHIPMENT_ALREADY_EXIST_MESSAGE});
        }
        log.info("Validator createShipmentCommand fin");
    }

    public void cancel(CancelShipmentCommand cancelShipmentCommand) {
        log.info("Validator cancelShipmentCommand init");
        //validation exist
        Optional<ShipmentEntity> optionalShipmentEntityValidateExist = this.shipmentRepository.findByShipmentIdAndStatus(cancelShipmentCommand.getShipmentId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalShipmentEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.SHIPMENT_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.SHIPMENT_NOT_FOUND_MESSAGE});
        }
        log.info("Validator cancelShipmentCommand fin");
    }

}
