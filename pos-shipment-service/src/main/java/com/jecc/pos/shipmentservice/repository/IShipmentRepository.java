package com.jecc.pos.shipmentservice.repository;

import com.jecc.pos.entities.ShipmentEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Resource
public interface IShipmentRepository extends JpaRepository<ShipmentEntity, String> {

    Optional<ShipmentEntity> findByShipmentIdAndStatus(String shipmentId, String status);

    @Query(
            value = "select c from ShipmentEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%')  ) " +
                    "order by c.createAt desc "
    )
    Optional<List<ShipmentEntity>> findEntities(
            @Param("status") String status,
            @Param("filter") String filter,
            Pageable pageable);

    @Query(
            value = "select count(c) from ShipmentEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%')  ) "
    )
    Long findCountEntities(
            @Param("status") String status,
            @Param("filter") String filter);

    @Query(
            value = "select c from ShipmentEntity c " +
                    "where c.status = :status and (c.orderId = :orderId) ")
    Optional<ShipmentEntity> findByOrderId(@Param("orderId") String orderId, @Param("status") String status);

}
