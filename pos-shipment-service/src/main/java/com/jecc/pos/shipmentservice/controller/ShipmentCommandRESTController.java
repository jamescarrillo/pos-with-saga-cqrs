package com.jecc.pos.shipmentservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.commands.shipment.cancel.CancelShipmentCommand;
import com.jecc.pos.commons.commands.shipment.create.CreateShipmentCommand;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("shipment")
@Log4j2
public class ShipmentCommandRESTController {

    @Resource
    private Pipeline pipeline;

    @PostMapping
    public ResponseEntity<ApiResponse<CreateShipmentCommand>> create(@RequestBody CreateShipmentCommand createShipmentCommand) {
        return new ResponseEntity<>(this.pipeline.send(createShipmentCommand), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ApiResponse<CancelShipmentCommand>> update(@RequestBody CancelShipmentCommand cancelShipmentCommand) {
        return new ResponseEntity<>(this.pipeline.send(cancelShipmentCommand), HttpStatus.CREATED);
    }


}
