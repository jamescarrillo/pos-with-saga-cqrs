package com.jecc.pos.commons.querys.product;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.BasePagination;
import com.jecc.pos.util.api.Pagination;

public class ProductPaginationQueryProjection extends BasePagination implements Command<ApiResponse<Pagination<ProductDTO>>> {

}
