package com.jecc.pos.commons.commands.inventory.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateInventoryCommand extends InventoryDTO implements Command<ApiResponse<CreateInventoryCommand>>, Serializable {

    @TargetAggregateIdentifier
    private String inventoryId;

}
