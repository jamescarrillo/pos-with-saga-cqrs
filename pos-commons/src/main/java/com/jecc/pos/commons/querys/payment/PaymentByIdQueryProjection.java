package com.jecc.pos.commons.querys.payment;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.PaymentDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentByIdQueryProjection implements Command<ApiResponse<PaymentDTO>> {

    private String paymentId;
}
