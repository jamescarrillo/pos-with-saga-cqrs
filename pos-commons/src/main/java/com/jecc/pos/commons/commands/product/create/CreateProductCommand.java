package com.jecc.pos.commons.commands.product.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.*;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductCommand extends ProductDTO implements Command<ApiResponse<CreateProductCommand>>, Serializable {

    @TargetAggregateIdentifier
    private String productId;

}
