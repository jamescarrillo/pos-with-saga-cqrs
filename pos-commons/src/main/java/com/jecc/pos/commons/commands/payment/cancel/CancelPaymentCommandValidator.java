package com.jecc.pos.commons.commands.payment.cancel;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class CancelPaymentCommandValidator extends AbstractValidator<CancelPaymentCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CancelPaymentCommandValidator");
        ruleFor(CancelPaymentCommand::getPaymentId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El paymentId no debe ser nulo o vacío")
                .withFieldName("paymentId");
        ruleFor(CancelPaymentCommand::getUserUpdate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de cancelación")
                .withFieldName("userUpdate");

    }

}
