package com.jecc.pos.commons.events.shipment;

import com.jecc.pos.types.ShipmentDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class CancelShipmentEvent extends ShipmentDTO implements Serializable {

}
