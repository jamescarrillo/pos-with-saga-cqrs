package com.jecc.pos.commons.commands.order.cancel;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class CancelOrderCommandValidator extends AbstractValidator<CancelOrderCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CancelOrderCommandValidator");
        ruleFor(CancelOrderCommand::getOrderId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El orderId no debe ser nulo o vacío")
                .withFieldName("orderId");
        ruleFor(CancelOrderCommand::getUserUpdate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de cancelación")
                .withFieldName("userUpdate");

    }

}
