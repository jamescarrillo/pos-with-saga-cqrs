package com.jecc.pos.commons.commands.inventory.update;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;
import com.jecc.pos.commons.commands.product.create.CreateProductCommand;

import java.util.Objects;
import java.util.function.Predicate;

public class UpdateInventoryCommandValidator extends AbstractValidator<UpdateInventoryCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("UpdateInventoryCommandValidator");
        ruleFor(UpdateInventoryCommand::getInventoryId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El inventoryId no debe ser nulo o vacío")
                .withFieldName("inventoryId");
        ruleFor(UpdateInventoryCommand::getStock)
                .must(Predicate.not(Objects::isNull))
                .withMessage("El stock no debe ser nulo")
                .withFieldName("stock");
        ruleFor(UpdateInventoryCommand::getUserUpdate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de actualización")
                .withFieldName("userUpdate");

    }

}
