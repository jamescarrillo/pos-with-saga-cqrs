package com.jecc.pos.commons.commands.order.complete;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class CompleteOrderCommandValidator extends AbstractValidator<CompleteOrderCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CancelOrderCommandValidator");
        ruleFor(CompleteOrderCommand::getOrderId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El orderId no debe ser nulo o vacío")
                .withFieldName("orderId");
        ruleFor(CompleteOrderCommand::getUserUpdate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de actaulización")
                .withFieldName("userUpdate");

    }

}
