package com.jecc.pos.commons.commands.product.update;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class UpdateProductCommandValidator extends AbstractValidator<UpdateProductCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("UpdateProductCommandValidator");
        ruleFor(UpdateProductCommand::getProductId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El productId no debe ser nulo o vacío")
                .withFieldName("productId");
        ruleFor(UpdateProductCommand::getCode)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El código no debe ser nulo o vacío")
                .withFieldName("code");
        ruleFor(UpdateProductCommand::getName)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El nombre no debe ser nulo o vacío")
                .withFieldName("name");
        ruleFor(UpdateProductCommand::getUserUpdate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de actualización")
                .withFieldName("userUpdate");

    }

}
