package com.jecc.pos.commons.commands.order.cancel;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class CancelOrderCommandHandler implements Command.Handler<CancelOrderCommand, ApiResponse<CancelOrderCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CancelOrderCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CancelOrderCommand> handle(CancelOrderCommand cancelOrderCommand) {
        log.info("Handling cancelOrderCommand...");
        cancelOrderCommand.setUpdateAt(LocalDateTime.now());
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(cancelOrderCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CancelOrderCommand>()
                .builSuccesful(cancelOrderCommand)
                .build();
    }
}
