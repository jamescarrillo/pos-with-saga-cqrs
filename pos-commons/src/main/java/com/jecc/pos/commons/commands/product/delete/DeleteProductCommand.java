package com.jecc.pos.commons.commands.product.delete;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeleteProductCommand extends ProductDTO implements Command<ApiResponse<DeleteProductCommand>> {

    @TargetAggregateIdentifier
    private String productId;

}
