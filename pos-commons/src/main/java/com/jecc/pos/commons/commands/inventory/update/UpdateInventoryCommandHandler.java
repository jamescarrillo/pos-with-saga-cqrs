package com.jecc.pos.commons.commands.inventory.update;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class UpdateInventoryCommandHandler implements Command.Handler<UpdateInventoryCommand, ApiResponse<UpdateInventoryCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public UpdateInventoryCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<UpdateInventoryCommand> handle(UpdateInventoryCommand updateInventoryCommand) {
        log.info("Handling updateProductCommand...");
        updateInventoryCommand.setUpdateAt(LocalDateTime.now());
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(updateInventoryCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<UpdateInventoryCommand>()
                .builSuccesful(updateInventoryCommand)
                .build();
    }
}
