package com.jecc.pos.commons.commands.inventory.delete;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeleteInventoryCommand extends InventoryDTO implements Command<ApiResponse<DeleteInventoryCommand>> {

    @TargetAggregateIdentifier
    private String inventoryId;

}
