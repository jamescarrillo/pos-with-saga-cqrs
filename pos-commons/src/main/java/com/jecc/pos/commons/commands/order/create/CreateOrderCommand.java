package com.jecc.pos.commons.commands.order.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrderCommand extends OrderDTO implements Command<ApiResponse<CreateOrderCommand>>, Serializable {

    @TargetAggregateIdentifier
    private String orderId;

}
