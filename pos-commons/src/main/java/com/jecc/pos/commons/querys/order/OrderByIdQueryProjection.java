package com.jecc.pos.commons.querys.order;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderByIdQueryProjection implements Command<ApiResponse<OrderDTO>> {

    private String orderId;
}
