package com.jecc.pos.commons.commands.shipment.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.types.ShipmentDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateShipmentCommand extends ShipmentDTO implements Command<ApiResponse<CreateShipmentCommand>>, Serializable {

    @TargetAggregateIdentifier
    private String shipmentId;

}
