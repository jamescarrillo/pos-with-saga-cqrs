package com.jecc.pos.commons.commands.payment.create;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class CreatePaymentCommandValidator extends AbstractValidator<CreatePaymentCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CreatePaymentCommandValidator");
        ruleFor(CreatePaymentCommand::getOrderId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El orderId no debe ser nulo o vacío")
                .withFieldName("orderId");
        ruleFor(CreatePaymentCommand::getUserCreate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de creación")
                .withFieldName("userCreate");
    }

}
