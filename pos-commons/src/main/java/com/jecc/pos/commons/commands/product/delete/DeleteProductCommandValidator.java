package com.jecc.pos.commons.commands.product.delete;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class DeleteProductCommandValidator extends AbstractValidator<DeleteProductCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("DeleteProductCommandValidator");
        ruleFor(DeleteProductCommand::getProductId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El productId no debe ser nulo o vacío")
                .withFieldName("productId");
        ruleFor(DeleteProductCommand::getUserDelete)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de eliminación")
                .withFieldName("userDelete");

    }

}
