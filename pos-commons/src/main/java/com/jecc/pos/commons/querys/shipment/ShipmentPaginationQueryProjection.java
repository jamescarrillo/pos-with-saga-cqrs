package com.jecc.pos.commons.querys.shipment;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.ShipmentDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.BasePagination;
import com.jecc.pos.util.api.Pagination;

public class ShipmentPaginationQueryProjection extends BasePagination implements Command<ApiResponse<Pagination<ShipmentDTO>>> {

}
