package com.jecc.pos.commons.querys.payment;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.PaymentDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.BasePagination;
import com.jecc.pos.util.api.Pagination;

public class PaymentPaginationQueryProjection extends BasePagination implements Command<ApiResponse<Pagination<PaymentDTO>>> {

}
