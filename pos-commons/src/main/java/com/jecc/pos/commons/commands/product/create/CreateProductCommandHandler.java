package com.jecc.pos.commons.commands.product.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
@Slf4j
public class CreateProductCommandHandler implements Command.Handler<CreateProductCommand, ApiResponse<CreateProductCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CreateProductCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CreateProductCommand> handle(CreateProductCommand createProductCommand) {
        log.info("Handling createProductCommand...");
        createProductCommand.setProductId(UUID.randomUUID().toString());
        createProductCommand.setStatus(ConstantsLibrary.STATUS_CREATED);
        createProductCommand.setCreateAt(LocalDateTime.now());
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(createProductCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CreateProductCommand>()
                .builSuccesful(createProductCommand)
                .build();
    }
}
