package com.jecc.pos.commons.commands.shipment.cancel;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class CancelShipmentCommandValidator extends AbstractValidator<CancelShipmentCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CancelShipmentCommandValidator");
        ruleFor(CancelShipmentCommand::getShipmentId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El shipmentId no debe ser nulo o vacío")
                .withFieldName("shipmentId");
        ruleFor(CancelShipmentCommand::getUserUpdate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de cancelación")
                .withFieldName("userUpdate");

    }

}
