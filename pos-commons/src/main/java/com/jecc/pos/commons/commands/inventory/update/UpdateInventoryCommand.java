package com.jecc.pos.commons.commands.inventory.update;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateInventoryCommand extends InventoryDTO implements Command<ApiResponse<UpdateInventoryCommand>> {

    @TargetAggregateIdentifier
    private String inventoryId;

}
