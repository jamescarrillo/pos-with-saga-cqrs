package com.jecc.pos.commons.commands.shipment.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
@Slf4j
public class CreateShipmentCommandHandler implements Command.Handler<CreateShipmentCommand, ApiResponse<CreateShipmentCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CreateShipmentCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CreateShipmentCommand> handle(CreateShipmentCommand createShipmentCommand) {
        log.info("Handling createShipmentCommand...");
        createShipmentCommand.setShipmentId(UUID.randomUUID().toString());
        createShipmentCommand.setCode("SHIP" + System.currentTimeMillis());
        createShipmentCommand.setStatus(ConstantsLibrary.STATUS_CREATED);
        createShipmentCommand.setCreateAt(LocalDateTime.now());
        createShipmentCommand.setShipmentStatus("DELIVERED");
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(createShipmentCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CreateShipmentCommand>()
                .builSuccesful(createShipmentCommand)
                .build();
    }
}
