package com.jecc.pos.commons.events.inventory;

import com.jecc.pos.types.InventoryDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class CreateInventoryEvent extends InventoryDTO {

}
