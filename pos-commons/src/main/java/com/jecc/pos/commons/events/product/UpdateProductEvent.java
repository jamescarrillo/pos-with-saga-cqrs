package com.jecc.pos.commons.events.product;

import com.jecc.pos.types.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class UpdateProductEvent extends ProductDTO {

}
