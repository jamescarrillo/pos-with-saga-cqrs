package com.jecc.pos.commons.events.order;

import com.jecc.pos.types.OrderDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class CompleteOrderEvent extends OrderDTO implements Serializable {

}
