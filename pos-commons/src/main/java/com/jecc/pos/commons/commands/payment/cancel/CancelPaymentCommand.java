package com.jecc.pos.commons.commands.payment.cancel;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.types.PaymentDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CancelPaymentCommand extends PaymentDTO implements Command<ApiResponse<CancelPaymentCommand>>, Serializable {
    @TargetAggregateIdentifier
    private String paymentId;
}
