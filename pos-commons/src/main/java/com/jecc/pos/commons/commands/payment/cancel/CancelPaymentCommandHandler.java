package com.jecc.pos.commons.commands.payment.cancel;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class CancelPaymentCommandHandler implements Command.Handler<CancelPaymentCommand, ApiResponse<CancelPaymentCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CancelPaymentCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CancelPaymentCommand> handle(CancelPaymentCommand cancelPaymentCommand) {
        log.info("Handling cancelPaymentCommand...");
        cancelPaymentCommand.setUpdateAt(LocalDateTime.now());
        try {
            log.info("start command");
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(cancelPaymentCommand));
            log.info("end command");
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CancelPaymentCommand>()
                .builSuccesful(cancelPaymentCommand)
                .build();
    }
}
