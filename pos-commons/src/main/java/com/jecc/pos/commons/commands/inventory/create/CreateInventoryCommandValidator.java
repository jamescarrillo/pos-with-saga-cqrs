package com.jecc.pos.commons.commands.inventory.create;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;
import com.jecc.pos.commons.commands.inventory.update.UpdateInventoryCommand;

import java.util.Objects;
import java.util.function.Predicate;

public class CreateInventoryCommandValidator extends AbstractValidator<CreateInventoryCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CreateInventoryCommandValidator");
        ruleFor(CreateInventoryCommand::getStock)
                .must(Predicate.not(Objects::isNull))
                .withMessage("El stock no debe ser nulo")
                .withFieldName("stock");
        ruleFor(CreateInventoryCommand::getUserCreate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de creación")
                .withFieldName("userCreate");
    }

}
