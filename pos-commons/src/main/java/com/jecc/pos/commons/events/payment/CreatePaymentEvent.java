package com.jecc.pos.commons.events.payment;

import com.jecc.pos.types.PaymentDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class CreatePaymentEvent extends PaymentDTO implements Serializable {

}
