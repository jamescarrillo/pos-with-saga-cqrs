package com.jecc.pos.commons.commands.shipment.create;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class CreateShipmentCommandValidator extends AbstractValidator<CreateShipmentCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CreateProductCommandValidator");
        ruleFor(CreateShipmentCommand::getOrderId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El orderId no debe ser nulo o vacío")
                .withFieldName("orderId");
        ruleFor(CreateShipmentCommand::getUserCreate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de creación")
                .withFieldName("userCreate");
    }

}
