package com.jecc.pos.commons.commands.payment.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
@Slf4j
public class CreatePaymentCommandHandler implements Command.Handler<CreatePaymentCommand, ApiResponse<CreatePaymentCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CreatePaymentCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CreatePaymentCommand> handle(CreatePaymentCommand createPaymentCommand) {
        log.info("Handling createPaymentCommand...");
        createPaymentCommand.setPaymentId(UUID.randomUUID().toString());
        createPaymentCommand.setCode("PAY" + System.currentTimeMillis());
        createPaymentCommand.setStatus(ConstantsLibrary.STATUS_CREATED);
        createPaymentCommand.setCreateAt(LocalDateTime.now());
        createPaymentCommand.setPaymentStatus("COMPLETED");
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(createPaymentCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CreatePaymentCommand>()
                .builSuccesful(createPaymentCommand)
                .build();
    }
}
