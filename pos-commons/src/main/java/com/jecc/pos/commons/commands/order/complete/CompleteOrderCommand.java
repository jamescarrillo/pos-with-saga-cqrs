package com.jecc.pos.commons.commands.order.complete;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompleteOrderCommand extends OrderDTO implements Command<ApiResponse<CompleteOrderCommand>>, Serializable {
    @TargetAggregateIdentifier
    private String orderId;
}
