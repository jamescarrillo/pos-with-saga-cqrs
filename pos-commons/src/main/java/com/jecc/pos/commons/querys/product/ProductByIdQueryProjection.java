package com.jecc.pos.commons.querys.product;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductByIdQueryProjection implements Command<ApiResponse<ProductDTO>> {

    private String productId;
}
