package com.jecc.pos.commons.commands.inventory.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
@Slf4j
public class CreateInventoryCommandHandler implements Command.Handler<CreateInventoryCommand, ApiResponse<CreateInventoryCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CreateInventoryCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CreateInventoryCommand> handle(CreateInventoryCommand createInventoryCommand) {
        log.info("Handling createInventoryCommand...");
        createInventoryCommand.setInventoryId(UUID.randomUUID().toString());
        createInventoryCommand.setStatus(ConstantsLibrary.STATUS_CREATED);
        createInventoryCommand.setCreateAt(LocalDateTime.now());
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(createInventoryCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CreateInventoryCommand>()
                .builSuccesful(createInventoryCommand)
                .build();
    }
}
