package com.jecc.pos.commons.events.product;

import com.jecc.pos.types.ProductDTO;
import lombok.Getter;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.io.Serializable;

public class CreateProductEvent extends ProductDTO implements Serializable {

}
