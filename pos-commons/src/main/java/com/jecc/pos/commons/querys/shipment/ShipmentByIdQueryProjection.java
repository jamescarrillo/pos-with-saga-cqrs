package com.jecc.pos.commons.querys.shipment;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.ShipmentDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentByIdQueryProjection implements Command<ApiResponse<ShipmentDTO>> {

    private String shipmentId;
}
