package com.jecc.pos.commons.commands.inventory.delete;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;
import com.jecc.pos.commons.commands.inventory.create.CreateInventoryCommand;

import java.util.Objects;
import java.util.function.Predicate;

public class DeleteInventoryCommandValidator extends AbstractValidator<DeleteInventoryCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("DeleteInventoryCommandValidator");
        ruleFor(DeleteInventoryCommand::getInventoryId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El inventoryId no debe ser nulo o vacío")
                .withFieldName("inventoryId");
        ruleFor(DeleteInventoryCommand::getStock)
                .must(Predicate.not(Objects::isNull))
                .withMessage("El stock no debe ser nulo")
                .withFieldName("stock");
        ruleFor(DeleteInventoryCommand::getUserDelete)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de eliminación")
                .withFieldName("userDelete");

    }

}
