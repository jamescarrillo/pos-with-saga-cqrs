package com.jecc.pos.commons.querys.inventory;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.BasePagination;
import com.jecc.pos.util.api.Pagination;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryPaginationQueryProjection extends BasePagination implements Command<ApiResponse<Pagination<InventoryDTO>>> {

    private List<String> productsIds;

}
