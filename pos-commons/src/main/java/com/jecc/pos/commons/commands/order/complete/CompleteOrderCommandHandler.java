package com.jecc.pos.commons.commands.order.complete;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class CompleteOrderCommandHandler implements Command.Handler<CompleteOrderCommand, ApiResponse<CompleteOrderCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CompleteOrderCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CompleteOrderCommand> handle(CompleteOrderCommand completeOrderCommand) {
        log.info("Handling completeOrderCommand...");
        completeOrderCommand.setUpdateAt(LocalDateTime.now());
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(completeOrderCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CompleteOrderCommand>()
                .builSuccesful(completeOrderCommand)
                .build();
    }
}
