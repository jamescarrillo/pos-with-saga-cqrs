package com.jecc.pos.commons.commands.product.create;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.Objects;
import java.util.function.Predicate;

public class CreateProductCommandValidator extends AbstractValidator<CreateProductCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CreateProductCommandValidator");
        ruleFor(CreateProductCommand::getCode)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El código no debe ser nulo o vacío")
                .withFieldName("code");
        ruleFor(CreateProductCommand::getName)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El nombre no debe ser nulo o vacío")
                .withFieldName("name");
        ruleFor(CreateProductCommand::getUserCreate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de creación")
                .withFieldName("userCreate");
    }

}
