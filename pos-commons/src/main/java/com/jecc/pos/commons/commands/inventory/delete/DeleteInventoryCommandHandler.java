package com.jecc.pos.commons.commands.inventory.delete;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class DeleteInventoryCommandHandler implements Command.Handler<DeleteInventoryCommand, ApiResponse<DeleteInventoryCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public DeleteInventoryCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<DeleteInventoryCommand> handle(DeleteInventoryCommand deleteInventoryCommand) {
        log.info("Handling deleteProductCommand...");
        deleteInventoryCommand.setDeleteAt(LocalDateTime.now());
        deleteInventoryCommand.setStatus(ConstantsLibrary.STATUS_DELETED);
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(deleteInventoryCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<DeleteInventoryCommand>()
                .builSuccesful(deleteInventoryCommand)
                .build();
    }
}
