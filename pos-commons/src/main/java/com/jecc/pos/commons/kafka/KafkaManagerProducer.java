package com.jecc.pos.commons.kafka;

import com.jecc.pos.util.others.Event;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@Log4j2
public class KafkaManagerProducer {

    private final KafkaTemplate<String, Event<?>> producer;

    @Autowired
    public KafkaManagerProducer(KafkaTemplate<String, Event<?>> producer) {
        this.producer = producer;
    }

    public void publish(String topic, Event<?> event) {
        log.info("send kafka init");

        ListenableFuture<SendResult<String, Event<?>>> sendResultListenableFuture = this.producer.send(topic, event);

        sendResultListenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("unable to send message");
            }

            @Override
            public void onSuccess(SendResult<String, Event<?>> result) {
                log.info("send message=" + result.toString());
            }
        });
        log.info("send kafka fin");
    }
}
