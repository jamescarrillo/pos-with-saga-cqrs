package com.jecc.pos.commons.commands.order.create;


import an.awesome.pipelinr.Command;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
@Slf4j
public class CreateOrderCommandHandler implements Command.Handler<CreateOrderCommand, ApiResponse<CreateOrderCommand>> {

    private final CommandGateway commandGateway;

    @Autowired
    public CreateOrderCommandHandler(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public ApiResponse<CreateOrderCommand> handle(CreateOrderCommand createOrderCommand) {
        log.info("Handling createOrderCommand...");
        createOrderCommand.setOrderId(UUID.randomUUID().toString());
        createOrderCommand.setCode("OR" + System.currentTimeMillis());
        createOrderCommand.setStatus(ConstantsLibrary.STATUS_CREATED);
        createOrderCommand.setCreateAt(LocalDateTime.now());
        createOrderCommand.setOrderStatus(ConstantsLibrary.STATUS_CREATED);
        try {
            this.commandGateway.sendAndWait(new GenericCommandMessage<>(createOrderCommand));
        } catch (CommandExecutionException ex) {
            throw new CustomErrorException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getDetails(), ex.getMessage());
        }
        return new ApiResponse.Builder<CreateOrderCommand>()
                .builSuccesful(createOrderCommand)
                .build();
    }
}
