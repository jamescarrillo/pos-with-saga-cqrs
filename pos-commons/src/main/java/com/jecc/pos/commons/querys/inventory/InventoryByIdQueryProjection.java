package com.jecc.pos.commons.querys.inventory;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryByIdQueryProjection implements Command<ApiResponse<InventoryDTO>> {

    private String inventoryId;
}
