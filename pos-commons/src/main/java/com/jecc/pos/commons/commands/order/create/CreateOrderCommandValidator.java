package com.jecc.pos.commons.commands.order.create;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.predicate.StringPredicate;

import java.util.function.Predicate;

public class CreateOrderCommandValidator extends AbstractValidator<CreateOrderCommand> {

    @Override
    public void rules() {
        setPropertyOnContext("CreateOrderCommandValidator");
        ruleFor(CreateOrderCommand::getProductId)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El productId no debe ser nulo o vacío")
                .withFieldName("productId");
        ruleFor(CreateOrderCommand::getCustomerCode)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El customerCode no debe ser nulo o vacío")
                .withFieldName("customerCode");
        ruleFor(CreateOrderCommand::getCustomerName)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("El customerName no debe ser nulo o vacío")
                .withFieldName("customerName");
        ruleFor(CreateOrderCommand::getUserCreate)
                .must(Predicate.not(StringPredicate.stringEmptyOrNull()))
                .withMessage("No se especificó usuario de creación")
                .withFieldName("userCreate");
    }

}
