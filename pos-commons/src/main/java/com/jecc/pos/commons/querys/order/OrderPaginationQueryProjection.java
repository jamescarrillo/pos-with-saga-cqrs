package com.jecc.pos.commons.querys.order;

import an.awesome.pipelinr.Command;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.BasePagination;
import com.jecc.pos.util.api.Pagination;

public class OrderPaginationQueryProjection extends BasePagination implements Command<ApiResponse<Pagination<OrderDTO>>> {

}
