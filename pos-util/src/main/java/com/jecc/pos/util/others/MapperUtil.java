package com.jecc.pos.util.others;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MapperUtil {

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
    }

    public static <T> T deserialize(Object data, Class<T> tClass){
        return objectMapper.convertValue(data, tClass);
    }

}
