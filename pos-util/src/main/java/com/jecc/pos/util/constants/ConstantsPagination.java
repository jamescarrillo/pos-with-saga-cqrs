package com.jecc.pos.util.constants;

public class ConstantsPagination {

    private ConstantsPagination() {

    }

    public static final String PATTERN = "dd/MM/yyyy";
    public static final String PAGE = "0";
    public static final String SIZE = "10";
    public static final String MAX_SIZE = "1000000";
    public static final String FILTER = "";
    public static final String DEFAULT_FILTER = "";
}
