package com.jecc.pos.util.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse<T> implements Serializable {

    private boolean success;
    private List<String> customMessages;
    private String customCode;
    private T data;
    private int code;
    private String status;

    public static class Builder<T> {

        private boolean success;
        private List<String> customMessages;
        private String customCode;
        private T data;
        private int code;
        private String status;

        public Builder() {
            //Init true
            this.success = false;
        }

        public Builder<T> success(boolean success) {
            this.success = success;
            return this;
        }

        public Builder<T> customMessages(List<String> customMessages) {
            this.customMessages = customMessages;
            return this;
        }

        public Builder<T> customCode(String customCode) {
            this.customCode = customCode;
            return this;
        }

        public Builder<T> data(T data) {
            this.data = data;
            return this;
        }

        public Builder<T> code(int code) {
            this.code = code;
            return this;
        }

        public Builder<T> status(String status) {
            this.status = status;
            return this;
        }

        public Builder<T> buildWithError(String customCode, List<String> customMessages) {
            this.customCode = customCode;
            this.customMessages = customMessages;
            return this;
        }

        public Builder<T> builSuccesful(T data) {
            this.success = true;
            this.code = 200;
            this.data = data;
            return this;
        }

        public Builder<T> builSuccesful(T data, List<String> customMessages) {
            this.success = true;
            this.data = data;
            this.customMessages = customMessages;
            return this;
        }

        public ApiResponse<T> build() {
            ApiResponse<T> apiResponse = new ApiResponse<>();
            apiResponse.success = this.success;
            apiResponse.customCode = this.customCode;
            apiResponse.customMessages = this.customMessages;
            apiResponse.data = this.data;
            apiResponse.code = this.code;
            apiResponse.status = this.status;
            return apiResponse;
        }

    }

    public ApiResponse() {
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "success=" + success +
                ", customMessages='" + customMessages + '\'' +
                ", customCode='" + customCode + '\'' +
                ", data=" + data +
                ", code=" + code +
                ", status='" + status + '\'' +
                '}';
    }
}
