package com.jecc.pos.util.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pagination<T> implements Serializable {

    private long countFilter = 0;
    private int totalPages;

    private List<T> list = new ArrayList<>();

    public int processAndGetTotalPages(int size) {
        if (this.countFilter > 0) {
            this.totalPages = (int) this.countFilter / size;
            if (this.countFilter % size > 0) {
                this.totalPages++;
            }
        }
        return totalPages;
    }

}
