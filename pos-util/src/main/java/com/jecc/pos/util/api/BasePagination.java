package com.jecc.pos.util.api;

import lombok.Data;

@Data
public class BasePagination {

    private String filter;
    private int page;
    private int size;

}
