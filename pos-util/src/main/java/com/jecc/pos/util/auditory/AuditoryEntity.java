package com.jecc.pos.util.auditory;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
@Data
public class AuditoryEntity {

    @Column(length = 8)
    private String status; // CREATED, DELETED
    @Column(name = "create_at")
    private LocalDateTime createAt;
    @Column(name = "update_at")
    private LocalDateTime updateAt;
    @Column(name = "delete_at")
    private LocalDateTime deleteAt;
    @Column(length = 50, name = "user_create")
    private String userCreate;
    @Column(length = 50, name = "user_update")
    private String userUpdate;
    @Column(length = 50, name = "user_delete")
    private String userDelete;

}
