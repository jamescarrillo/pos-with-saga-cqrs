package com.jecc.pos.util.others;

public enum EventType {
	CREATED, UPDATED, DELETED
}
