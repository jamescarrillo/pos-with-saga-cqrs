package com.jecc.pos.util.others;

public interface IntegrationDTO<T> {

    T getDTO();

}
