package com.jecc.pos.util.constants;

public class ErrorsConstants {

    public static final String PRODUCT_NOT_FOUND_CODE = "PRODUCT_NOT_FOUND";
    public static final String PRODUCT_NOT_FOUND_MESSAGE = "Producto no encontrado";

    public static final String PRODUCT_ALREADY_EXIST_CODE = "PRODUCT_ALREADY_EXIST";
    public static final String PRODUCT_ALREADY_EXIST_MESSAGE = "Ya existe un producto con el código o nombre ingresado";

    public static final String INVENTORY_NOT_FOUND_CODE = "INVENTORY_NOT_FOUND";
    public static final String INVENTORY_NOT_FOUND_MESSAGE = "Inventario no encontrado";

    public static final String INVENTORY_ALREADY_EXIST_CODE = "INVENTORY_ALREADY_EXIST";
    public static final String INVENTORY_ALREADY_EXIST_MESSAGE = "Ya existe un inventario para el producto ingresado";

    public static final String ORDER_NOT_FOUND_CODE = "ORDER_NOT_FOUND_CODE";
    public static final String ORDER_NOT_FOUND_MESSAGE = "Orden no encontrada";

    public static final String ORDER_ALREADY_EXIST_CODE = "ORDER_ALREADY_EXIST_CODE";
    public static final String ORDER_ALREADY_EXIST_MESSAGE = "Ya existe una order con el código ingresado";

    public static final String PAYMENT_NOT_FOUND_CODE = "PAYMENT_NOT_FOUND_CODE";
    public static final String PAYMENT_NOT_FOUND_MESSAGE = "Pago no encontrado";
    public static final String PAYMENT_ALREADY_EXIST_CODE = "PAYMENT_ALREADY_EXIST_CODE";
    public static final String PAYMENT_ALREADY_EXIST_MESSAGE = "Ya existe una pago para la orden ingresada";

    public static final String SHIPMENT_NOT_FOUND_CODE = "SHIPMENT_NOT_FOUND_CODE";
    public static final String SHIPMENT_NOT_FOUND_MESSAGE = "Entrega no encontrada";
    public static final String SHIPMENT_ALREADY_EXIST_CODE = "SHIPMENT_ALREADY_EXIST_CODE";
    public static final String SHIPMENT_ALREADY_EXIST_MESSAGE = "Ya existe una entrega con la orden ingresada";


}
