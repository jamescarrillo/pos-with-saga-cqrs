package com.jecc.pos.util.constants;

public class ConstantsLibrary {

    public static final String STATUS_CREATED = "CREATED";
    public static final String STATUS_DELETED = "DELETED";
}
