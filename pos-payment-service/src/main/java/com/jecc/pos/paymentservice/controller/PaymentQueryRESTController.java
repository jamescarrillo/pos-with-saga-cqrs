package com.jecc.pos.paymentservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.querys.payment.PaymentByIdQueryProjection;
import com.jecc.pos.commons.querys.payment.PaymentPaginationQueryProjection;
import com.jecc.pos.types.PaymentDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsPagination;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("payment")
@Log4j2
public class PaymentQueryRESTController {

    @Resource
    private Pipeline pipeline;

    @GetMapping
    public ResponseEntity<ApiResponse<Pagination<PaymentDTO>>> getPagination(
            @RequestParam(defaultValue = ConstantsPagination.DEFAULT_FILTER) String filter,
            @RequestParam(defaultValue = ConstantsPagination.PAGE) int page,
            @RequestParam(defaultValue = ConstantsPagination.SIZE) int size) {
        PaymentPaginationQueryProjection paymentPaginationQueryProjection = new PaymentPaginationQueryProjection();
        paymentPaginationQueryProjection.setFilter(filter);
        paymentPaginationQueryProjection.setPage(page);
        paymentPaginationQueryProjection.setSize(size);
        return new ResponseEntity<>(this.pipeline.send(paymentPaginationQueryProjection), HttpStatus.ACCEPTED);
    }

    @GetMapping("{id}")
    public ResponseEntity<ApiResponse<PaymentDTO>> getById(@PathVariable String id) {
        return new ResponseEntity<>(this.pipeline.send(new PaymentByIdQueryProjection(id)), HttpStatus.ACCEPTED);
    }

}
