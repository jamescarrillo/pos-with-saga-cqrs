package com.jecc.pos.paymentservice.repository;

import com.jecc.pos.entities.PaymentEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Resource
public interface IPaymentRepository extends JpaRepository<PaymentEntity, String> {

    Optional<PaymentEntity> findByPaymentIdAndStatus(String paymentId, String status);

    @Query(
            value = "select c from PaymentEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%')  ) " +
                    "order by c.createAt desc "
    )
    Optional<List<PaymentEntity>> findEntities(
            @Param("status") String status,
            @Param("filter") String filter,
            Pageable pageable);

    @Query(
            value = "select count(c) from PaymentEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%')  ) "
    )
    Long findCountEntities(
            @Param("status") String status,
            @Param("filter") String filter);

    @Query(
            value = "select c from PaymentEntity c " +
                    "where c.status = :status and (c.orderId = :orderId) ")
    Optional<PaymentEntity> findByOrderId(@Param("orderId") String orderId, @Param("status") String status);

}
