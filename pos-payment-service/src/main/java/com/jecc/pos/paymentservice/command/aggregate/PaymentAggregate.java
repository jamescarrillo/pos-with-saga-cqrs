package com.jecc.pos.paymentservice.command.aggregate;

import com.jecc.pos.commons.commands.payment.cancel.CancelPaymentCommand;
import com.jecc.pos.commons.commands.payment.create.CreatePaymentCommand;
import com.jecc.pos.commons.events.payment.CancelPaymentEvent;
import com.jecc.pos.commons.events.payment.CreatePaymentEvent;
import com.jecc.pos.types.PaymentDTO;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Aggregate
@Slf4j
public class PaymentAggregate extends PaymentDTO {

    @AggregateIdentifier
    private String paymentId;

    public PaymentAggregate() {
    }

    @CommandHandler
    public PaymentAggregate(CreatePaymentCommand createPaymentCommand,
                            @Autowired PaymentAggregateValidator paymentAggregateValidator) {
        log.info("createPaymentCommand received.");
        paymentAggregateValidator.create(createPaymentCommand);
        CreatePaymentEvent createPaymentEvent = new CreatePaymentEvent();
        BeanUtils.copyProperties(createPaymentCommand, createPaymentEvent);
        AggregateLifecycle.apply(createPaymentEvent);
    }

    @CommandHandler
    public void on(CancelPaymentCommand cancelPaymentCommand,
                   @Autowired PaymentAggregateValidator paymentAggregateValidator) {
        log.info("cancelPaymentCommand received.");
        paymentAggregateValidator.cancel(cancelPaymentCommand);
        CancelPaymentEvent cancelPaymentEvent = new CancelPaymentEvent();
        BeanUtils.copyProperties(cancelPaymentCommand, cancelPaymentCommand);
        AggregateLifecycle.apply(cancelPaymentEvent);
    }

    @EventSourcingHandler
    public void on(CreatePaymentEvent createPaymentEvent) {
        log.info("createPaymentEvent occurred.");
        this.paymentId = createPaymentEvent.getPaymentId();
        this.setCode(createPaymentEvent.getCode());
        this.setPaymentStatus(createPaymentEvent.getPaymentStatus());
        this.setOrderId(createPaymentEvent.getOrderId());
        this.setStatus(createPaymentEvent.getStatus());
        this.setCreateAt(createPaymentEvent.getCreateAt());
        this.setUpdateAt(createPaymentEvent.getUpdateAt());
        this.setDeleteAt(createPaymentEvent.getDeleteAt());
        this.setUserCreate(createPaymentEvent.getUserCreate());
        this.setUserUpdate(createPaymentEvent.getUserUpdate());
        this.setUserDelete(createPaymentEvent.getUserDelete());
    }


    @EventSourcingHandler
    public void on(CancelPaymentEvent cancelPaymentEvent) {
        log.info("cancelPaymentEvent occurred.");
        this.paymentId = cancelPaymentEvent.getPaymentId();
        this.setPaymentStatus(cancelPaymentEvent.getPaymentStatus());
        this.setUpdateAt(cancelPaymentEvent.getUpdateAt());
        this.setUserUpdate(cancelPaymentEvent.getUserUpdate());
    }

}
