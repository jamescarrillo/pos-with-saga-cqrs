package com.jecc.pos.paymentservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.payment.PaymentByIdQueryProjection;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.entities.PaymentEntity;
import com.jecc.pos.paymentservice.repository.IPaymentRepository;
import com.jecc.pos.types.PaymentDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PaymentByIdQueryHandler implements Command.Handler<PaymentByIdQueryProjection, ApiResponse<PaymentDTO>> {

    private final IPaymentRepository PaymentRepository;

    @Autowired
    public PaymentByIdQueryHandler(IPaymentRepository PaymentRepository) {
        this.PaymentRepository = PaymentRepository;
    }

    @Override
    public ApiResponse<PaymentDTO> handle(PaymentByIdQueryProjection PaymentByIdQueryProjection) {
        Optional<PaymentEntity> optionalPaymentEntity = this.PaymentRepository.findByPaymentIdAndStatus(
                PaymentByIdQueryProjection.getPaymentId(), ConstantsLibrary.STATUS_CREATED);
        optionalPaymentEntity.orElseThrow(() ->
                new CustomErrorException(
                        HttpStatus.NOT_FOUND,
                        ErrorsConstants.PAYMENT_NOT_FOUND_CODE,
                        new String[]{ErrorsConstants.PAYMENT_NOT_FOUND_MESSAGE},
                        ErrorsConstants.PAYMENT_NOT_FOUND_CODE));
        return new ApiResponse.Builder<PaymentDTO>()
                .builSuccesful(optionalPaymentEntity.get().getDTO())
                .build();
    }
}
