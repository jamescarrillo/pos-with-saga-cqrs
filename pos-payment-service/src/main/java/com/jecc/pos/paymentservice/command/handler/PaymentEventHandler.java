package com.jecc.pos.paymentservice.command.handler;

import com.jecc.pos.commons.events.payment.CancelPaymentEvent;
import com.jecc.pos.commons.events.payment.CreatePaymentEvent;
import com.jecc.pos.entities.PaymentEntity;
import com.jecc.pos.paymentservice.repository.IPaymentRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class PaymentEventHandler {

    private final IPaymentRepository paymentRepository;

    @Autowired
    public PaymentEventHandler(IPaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @EventHandler
    public void on(CreatePaymentEvent createPaymentEvent) {
        log.info("Handling createPaymentEvent start...");
        PaymentEntity paymentEntity = new PaymentEntity();
        BeanUtils.copyProperties(createPaymentEvent, paymentEntity);
        this.paymentRepository.save(paymentEntity);
        log.info("Handling createPaymentEvent end...");
    }

    @EventHandler
    public void on(CancelPaymentEvent cancelPaymentEvent) {
        log.info("Handling cancelPaymentEvent start...");
        Optional<PaymentEntity> optionalPaymentEntity = this.paymentRepository.findByPaymentIdAndStatus(cancelPaymentEvent.getPaymentId(),
                ConstantsLibrary.STATUS_CREATED);
        if (optionalPaymentEntity.isPresent()) {
            PaymentEntity paymentEntity = optionalPaymentEntity.get();
            paymentEntity.setPaymentStatus(cancelPaymentEvent.getPaymentStatus());
            paymentEntity.setUpdateAt(cancelPaymentEvent.getUpdateAt());
            paymentEntity.setUserUpdate(cancelPaymentEvent.getUserUpdate());
            this.paymentRepository.save(paymentEntity);
        }
        log.info("Handling cancelPaymentEvent end...");
    }


}
