package com.jecc.pos.paymentservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.commands.payment.cancel.CancelPaymentCommand;
import com.jecc.pos.commons.commands.payment.create.CreatePaymentCommand;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("payment")
@Log4j2
public class PaymentCommandRESTController {

    @Resource
    private Pipeline pipeline;

    @PostMapping
    public ResponseEntity<ApiResponse<CreatePaymentCommand>> create(@RequestBody CreatePaymentCommand createPaymentCommand) {
        return new ResponseEntity<>(this.pipeline.send(createPaymentCommand), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ApiResponse<CancelPaymentCommand>> update(@RequestBody CancelPaymentCommand cancelPaymentCommand) {
        return new ResponseEntity<>(this.pipeline.send(cancelPaymentCommand), HttpStatus.CREATED);
    }


}
