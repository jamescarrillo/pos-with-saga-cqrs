package com.jecc.pos.paymentservice.command.aggregate;

import com.jecc.pos.commons.commands.payment.cancel.CancelPaymentCommand;
import com.jecc.pos.commons.commands.payment.create.CreatePaymentCommand;
import com.jecc.pos.entities.PaymentEntity;
import com.jecc.pos.paymentservice.repository.IPaymentRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class PaymentAggregateValidator {

    private final IPaymentRepository paymentRepository;

    @Autowired
    public PaymentAggregateValidator(IPaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public void create(CreatePaymentCommand createPaymentCommand) {
        log.info("Validator createPaymentCommand init");
        Optional<PaymentEntity> optionalPaymentEntity =
                this.paymentRepository.findByOrderId(
                        createPaymentCommand.getOrderId(),
                        ConstantsLibrary.STATUS_CREATED);
        if (optionalPaymentEntity.isPresent()) {
            throw new CommandExecutionException(ErrorsConstants.PAYMENT_ALREADY_EXIST_CODE, null, new String[]{ErrorsConstants.PAYMENT_ALREADY_EXIST_MESSAGE});
        }
        log.info("Validator createPaymentCommand fin");
    }

    public void cancel(CancelPaymentCommand cancelPaymentCommand) {
        log.info("Validator cancelPaymentCommand init");
        //validation exist
        Optional<PaymentEntity> optionalPaymentEntityValidateExist = this.paymentRepository.findByPaymentIdAndStatus(cancelPaymentCommand.getPaymentId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalPaymentEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.PAYMENT_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.PAYMENT_NOT_FOUND_MESSAGE});
        }
        log.info("Validator cancelPaymentCommand fin");
    }

}
