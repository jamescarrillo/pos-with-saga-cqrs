package com.jecc.pos.paymentservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.payment.PaymentPaginationQueryProjection;
import com.jecc.pos.entities.PaymentEntity;
import com.jecc.pos.paymentservice.repository.IPaymentRepository;
import com.jecc.pos.types.PaymentDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Log4j2
public class PaymentPaginationQueryHandler implements Command.Handler<PaymentPaginationQueryProjection, ApiResponse<Pagination<PaymentDTO>>> {

    private final IPaymentRepository paymentRepository;

    @Autowired
    public PaymentPaginationQueryHandler(IPaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Cacheable(value = "value", keyGenerator = "customKeyGenerator", cacheManager = "customCacheManager")
    @Override
    public ApiResponse<Pagination<PaymentDTO>> handle(PaymentPaginationQueryProjection paymentPaginationQueryProjection) {
        log.info("PaymentPaginationQueryHandler init");
        Pagination<PaymentDTO> pagination = new Pagination<>();
        Pageable pageable = PageRequest.of(paymentPaginationQueryProjection.getPage(), paymentPaginationQueryProjection.getSize());
        pagination.setCountFilter(
                this.paymentRepository.findCountEntities(ConstantsLibrary.STATUS_CREATED,
                        paymentPaginationQueryProjection.getFilter())
        );
        pagination.setTotalPages(pagination.processAndGetTotalPages(paymentPaginationQueryProjection.getSize()));
        if (pagination.getCountFilter() > 0) {
            Optional<List<PaymentEntity>> optionalPaymentEntities = this.paymentRepository.findEntities(
                    ConstantsLibrary.STATUS_CREATED,
                    paymentPaginationQueryProjection.getFilter(),
                    pageable);
            pagination.setList(optionalPaymentEntities.orElse(new ArrayList<>()).stream().map(PaymentEntity::getDTO).collect(Collectors.toList()));
        }
        log.info("PaymentPaginationQueryHandler fin");
        return new ApiResponse.Builder<Pagination<PaymentDTO>>()
                .builSuccesful(pagination)
                .build();
    }

}
