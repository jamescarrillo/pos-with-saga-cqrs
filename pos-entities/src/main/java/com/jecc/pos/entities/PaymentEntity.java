package com.jecc.pos.entities;

import com.jecc.pos.types.PaymentDTO;
import com.jecc.pos.util.auditory.AuditoryEntity;
import com.jecc.pos.util.others.IntegrationDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_payment")
@Getter
@Setter
public class PaymentEntity extends AuditoryEntity implements IntegrationDTO<PaymentDTO> {

    @Id
    @Column(length = 40, name = "payment_id")
    private String paymentId;
    @Column(length = 20)
    private String code;
    @Column(length = 20, name = "payment_status")
    private String paymentStatus; //COMPLETED CANCELLED
    @Column(length = 40, name = "order_id")
    private String orderId;

    @Override
    public PaymentDTO getDTO() {
        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.setPaymentId(this.paymentId);
        paymentDTO.setCode(this.code);
        paymentDTO.setPaymentStatus(this.paymentStatus);
        paymentDTO.setOrderId(this.orderId);
        paymentDTO.setStatus(this.getStatus());
        paymentDTO.setCreateAt(this.getCreateAt());
        paymentDTO.setUpdateAt(this.getUpdateAt());
        paymentDTO.setDeleteAt(this.getDeleteAt());
        paymentDTO.setUserCreate(this.getUserCreate());
        paymentDTO.setUserUpdate(this.getUserUpdate());
        paymentDTO.setUserDelete(this.getUserDelete());
        return paymentDTO;
    }
}
