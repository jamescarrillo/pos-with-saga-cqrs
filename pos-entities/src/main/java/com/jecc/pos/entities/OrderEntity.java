package com.jecc.pos.entities;

import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.auditory.AuditoryEntity;
import com.jecc.pos.util.others.IntegrationDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_order")
@Getter
@Setter
public class OrderEntity extends AuditoryEntity implements IntegrationDTO<OrderDTO> {

    @Id
    @Column(length = 40, name = "order_id")
    private String orderId;
    @Column(length = 20)
    private String code;
    @Column(length = 15, name = "customer_code")
    private String customerCode;
    @Column(length = 100, name = "customer_name")
    private String customerName;
    @Column(length = 40, name = "product_id")
    private String productId;
    @Column(length = 20, name = "order_status")
    private String orderStatus;
    private Double quantity;

    @Override
    public OrderDTO getDTO() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setOrderId(this.orderId);
        orderDTO.setCode(this.code);
        orderDTO.setCustomerCode(this.customerCode);
        orderDTO.setCustomerName(this.customerName);
        orderDTO.setProductId(this.productId);
        orderDTO.setQuantity(this.quantity);
        orderDTO.setStatus(this.getStatus());
        orderDTO.setCreateAt(this.getCreateAt());
        orderDTO.setUpdateAt(this.getUpdateAt());
        orderDTO.setDeleteAt(this.getDeleteAt());
        orderDTO.setUserCreate(this.getUserCreate());
        orderDTO.setUserUpdate(this.getUserUpdate());
        orderDTO.setUserDelete(this.getUserDelete());
        return orderDTO;
    }
}
