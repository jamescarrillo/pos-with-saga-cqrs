package com.jecc.pos.entities;

import com.jecc.pos.types.ShipmentDTO;
import com.jecc.pos.util.auditory.AuditoryEntity;
import com.jecc.pos.util.others.IntegrationDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_shipment")
@Getter
@Setter
public class ShipmentEntity extends AuditoryEntity implements IntegrationDTO<ShipmentDTO> {

    @Id
    @Column(length = 40, name = "shipment_id")
    private String shipmentId;
    @Column(length = 20)
    private String code;
    @Column(length = 20, name = "shipment_status")
    private String shipmentStatus; //PENDING, COMPLETED
    @Column(length = 40, name = "order_id")
    private String orderId;

    @Override
    public ShipmentDTO getDTO() {
        ShipmentDTO shipmentDTO = new ShipmentDTO();
        shipmentDTO.setShipmentId(this.shipmentId);
        shipmentDTO.setCode(this.code);
        shipmentDTO.setShipmentStatus(this.shipmentStatus);
        shipmentDTO.setOrderId(this.orderId);
        shipmentDTO.setStatus(this.getStatus());
        shipmentDTO.setCreateAt(this.getCreateAt());
        shipmentDTO.setUpdateAt(this.getUpdateAt());
        shipmentDTO.setDeleteAt(this.getDeleteAt());
        shipmentDTO.setUserCreate(this.getUserCreate());
        shipmentDTO.setUserUpdate(this.getUserUpdate());
        shipmentDTO.setUserDelete(this.getUserDelete());
        return shipmentDTO;
    }
}
