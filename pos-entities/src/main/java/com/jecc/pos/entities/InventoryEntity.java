package com.jecc.pos.entities;

import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.auditory.AuditoryEntity;
import com.jecc.pos.util.others.IntegrationDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_inventory")
@Getter
@Setter
public class InventoryEntity extends AuditoryEntity implements IntegrationDTO<InventoryDTO> {

    @Id
    @Column(length = 40, name = "inventory_id")
    private String inventoryId;
    private Double stock;

    @Column(name = "product_id")
    private String productId;

    @Override
    public InventoryDTO getDTO() {
        InventoryDTO inventoryDTO = new InventoryDTO();
        inventoryDTO.setInventoryId(this.inventoryId);
        inventoryDTO.setStock(this.stock);
        inventoryDTO.setStatus(this.getStatus());
        inventoryDTO.setCreateAt(this.getCreateAt());
        inventoryDTO.setUpdateAt(this.getUpdateAt());
        inventoryDTO.setDeleteAt(this.getDeleteAt());
        inventoryDTO.setUserCreate(this.getUserCreate());
        inventoryDTO.setUserUpdate(this.getUserUpdate());
        inventoryDTO.setUserDelete(this.getUserDelete());
        return inventoryDTO;
    }
}
