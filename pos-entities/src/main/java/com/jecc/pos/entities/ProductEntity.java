package com.jecc.pos.entities;

import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.auditory.AuditoryEntity;
import com.jecc.pos.util.others.IntegrationDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_product")
@Getter
@Setter
public class ProductEntity extends AuditoryEntity implements IntegrationDTO<ProductDTO> {

    @Id
    @Column(length = 40, name = "product_id")
    private String productId;
    @Column(length = 10)
    private String code;
    private String name;

    @Override
    public ProductDTO getDTO() {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductId(this.productId);
        productDTO.setCode(this.code);
        productDTO.setName(this.name);
        productDTO.setStatus(this.getStatus());
        productDTO.setCreateAt(this.getCreateAt());
        productDTO.setUpdateAt(this.getUpdateAt());
        productDTO.setDeleteAt(this.getDeleteAt());
        productDTO.setUserCreate(this.getUserCreate());
        productDTO.setUserUpdate(this.getUserUpdate());
        productDTO.setUserDelete(this.getUserDelete());
        return productDTO;
    }
}
