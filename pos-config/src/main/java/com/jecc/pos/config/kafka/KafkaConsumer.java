package com.jecc.pos.config.kafka;

import com.jecc.pos.util.others.Event;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Log4j2
@Configuration
public class KafkaConsumer {

    private String bootstrapAddress; // localhost:9092

    public KafkaConsumer() {
        this.bootstrapAddress = System.getenv("POS_KAFKA_HOST") == null ? "localhost" : System.getenv("POS_KAFKA_HOST");
        this.bootstrapAddress += ":" + (System.getenv("POS_KAFKA_PORT") == null ? "9092" : System.getenv("POS_KAFKA_PORT"));
        log.info("POS_KAFKA_HOST {}", System.getenv("POS_KAFKA_HOST"));
        log.info("POS_KAFKA_PORT {}", System.getenv("POS_KAFKA_PORT"));
        log.info("KafkaConsumer {}", this.bootstrapAddress);
    }

    @Bean
    public ConsumerFactory<String, Event<?>> consumerFactory() {
        Map<String, String> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapAddress);

        props.put(JsonSerializer.TYPE_MAPPINGS, "com.jecc:com.jecc.pos.util.others.Event");
        final JsonDeserializer<Event<?>> jsonDeserializer = new JsonDeserializer<>();
        return new DefaultKafkaConsumerFactory(
                props,
                new StringDeserializer(),
                jsonDeserializer);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Event<?>>
    kafkaListenerContainerFactory() {

        ConcurrentKafkaListenerContainerFactory<String, Event<?>> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

}
