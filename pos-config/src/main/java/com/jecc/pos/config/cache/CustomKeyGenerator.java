package com.jecc.pos.config.cache;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Configuration
public class CustomKeyGenerator implements KeyGenerator {
    static String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

    @SneakyThrows
    @Override
    public Object generate(Object target, Method method, Object... params) {
        ObjectMapper mapper = new ObjectMapper();
        String encode = "";
        if (params[0] != null) {
            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(params[0]);
            encode = encodeByMD5(jsonString);
        }

        return target.getClass().getName() + encode;
    }

    private static String encodeByMD5(String password) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("md5");
        byte[] results = digest.digest(password.getBytes());
        return byteArrayToString(results);
    }

    private static String byteArrayToString(byte[] results) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < results.length; i++) {
            //per byte to string
            sb.append(byteToString(results[i]));
        }
        return sb.toString();
    }

    private static Object byteToString(byte b) {
        int n = b;
        if (n < 0) {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hex[d1] + hex[d2];
    }
}
