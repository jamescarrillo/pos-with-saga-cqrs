package com.jecc.pos.config.exception;

import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class ExceptionGlobalResponse {

    @ExceptionHandler(CustomDataNotFoundException.class)
    public ResponseEntity<ApiResponse<Object>> handleCustomDataNotFoundExceptions(
            Exception e
    ) {
        log.warn("handleCustomDataNotFoundExceptions");
        HttpStatus status = HttpStatus.NOT_FOUND; // 404

        // converting the stack trace to String
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        String stackTrace = stringWriter.toString();

        log.error(stackTrace);

        return new ResponseEntity<>(
                new ApiResponse.Builder<>()
                        .success(false)
                        .code(status.value())
                        .status(status.name())
                        .customCode("UNHANDLED_EXCEPTION")
                        .customMessages(List.of(e.getMessage()))
                        .data(stackTrace)
                        .build(),
                status
        );
    }

    @ExceptionHandler(CustomParameterConstraintException.class)
    public ResponseEntity<ApiResponse<Object>> handleCustomParameterConstraintExceptions(
            Exception e
    ) {
        log.warn("handleCustomParameterConstraintExceptions");

        HttpStatus status = HttpStatus.BAD_REQUEST; // 400

        return new ResponseEntity<>(
                new ApiResponse.Builder<>()
                        .success(false)
                        .code(status.value())
                        .status(status.name())
                        .customCode("UNHANDLED_EXCEPTION")
                        .customMessages(List.of(e.getMessage()))
                        .data(null)
                        .build(),
                status
        );
    }

    @ExceptionHandler(CustomErrorException.class)
    public ResponseEntity<ApiResponse<Object>> handleCustomErrorExceptions(
            Exception e
    ) {
        log.warn("handleCustomErrorExceptions");
        // casting the generic Exception e to CustomErrorException
        CustomErrorException customErrorException = (CustomErrorException) e;

        HttpStatus status = customErrorException.getStatus();

        // converting the stack trace to String
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        customErrorException.printStackTrace(printWriter);
        String stackTrace = stringWriter.toString();

        log.error(stackTrace);

        Object gData = customErrorException.getData();
        List<String> errors = new ArrayList<>();
        if (gData != null) {
            if (gData.getClass().isArray()) {
                log.warn("mapping messages from array");
                String[] strings = Arrays.stream((Object[]) gData).map(Object::toString).toArray(String[]::new);
                errors = Arrays.asList(strings);
            } else {
                log.warn("mapping messages from Optional");
                Optional<Object> oGData = (Optional<Object>) gData;
                if (oGData.isPresent() && oGData.get().getClass().isArray()) {
                    String[] strings = Arrays.stream((Object[]) oGData.get()).map(Object::toString).toArray(String[]::new);
                    errors = Arrays.asList(strings);
                }
            }
        }
        log.info("all errors: {}", errors);
        errors = errors.stream().distinct().collect(Collectors.toList());
        log.info("errors distinct: {}", errors);
        return new ResponseEntity<>(
                new ApiResponse.Builder<>()
                        .success(false)
                        .code(status.value())
                        .status(status.name())
                        .customCode(customErrorException.getCustomCode() == null ? "UNHANDLED_EXCEPTION": customErrorException.getCustomCode())
                        .customMessages(errors)
                        .build(),
                status
        );
    }

    @ExceptionHandler(Exception.class) // exception handled
    public ResponseEntity<ApiResponse<Object>> handleExceptions(
            Exception e
    ) {
        log.warn("handleExceptions");
        // ... potential custom logic

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR; // 500

        // converting the stack trace to String
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        String stackTrace = stringWriter.toString();

        log.error(stackTrace);
        return new ResponseEntity<>(
                new ApiResponse.Builder<>()
                        .success(false)
                        .code(status.value())
                        .status(status.name())
                        .customCode("UNHANDLED_EXCEPTION")
                        .customMessages(List.of(e.getMessage()))
                        .build(),
                status
        );
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ApiResponse<Object>> runtimeException(RuntimeException e) {
        log.warn("runtimeException");

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR; // 500

        // converting the stack trace to String
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        String stackTrace = stringWriter.toString();

        log.error(stackTrace);
        return new ResponseEntity<>(
                new ApiResponse.Builder<>()
                        .success(false)
                        .code(status.value())
                        .status(status.name())
                        .customCode("UNHANDLED_EXCEPTION")
                        .customMessages(List.of(e.getMessage()))
                        .data(stackTrace)
                        .build(),
                status
        );
    }
/*
    //FileUpload
    @ExceptionHandler(MultipartException.class)
    public ResponseEntity<StatusResponse> handleError(MultipartException e, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new StatusResponse(false, e.getCause().getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR));

    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<StatusResponse> handleMaxSizeException(MaxUploadSizeExceededException exc) {
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StatusResponse(false, exc.getCause().getMessage(), HttpStatus.EXPECTATION_FAILED));
    }
 */
}
