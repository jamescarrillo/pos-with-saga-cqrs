package com.jecc.pos.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.extensions.mongo.DefaultMongoTemplate;
import org.axonframework.extensions.mongo.eventsourcing.eventstore.MongoEventStorageEngine;
import org.axonframework.serialization.Serializer;
import org.axonframework.serialization.json.JacksonSerializer;
import org.axonframework.spring.config.AxonConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@Slf4j
public class Axon {

    private String POS_MONGO_HOST;
    private String POS_MONGO_PORT;
    private String POS_MONGO_DB;
    private String POS_MONGO_USERNAME;
    private String POS_MONGO_PASSWORD;

    private ObjectMapper objectMapper() {
        var objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return objectMapper;
    }

    @Bean
    @Primary
    public Serializer serializer() {
        return new JacksonSerializer.Builder()
                .objectMapper(objectMapper())
                .build();
    }

    @Bean(name = "axonMongoClient")
    public MongoClient mongoClient() {
        //LOAD VARIABLES
        loadEnvironments();
        String url = "mongodb://" + POS_MONGO_USERNAME + ":" + POS_MONGO_PASSWORD + "@" +
                POS_MONGO_HOST + ":" + POS_MONGO_PORT + "/" + POS_MONGO_DB + "?authSource=admin";
        log.info("Url Mongo client: {}", url);
        ConnectionString connectionString = new ConnectionString(url);
        log.info("MONGO DATABASE: {}", connectionString.getDatabase());
        return MongoClients.create(connectionString);
    }

    @Bean
    public EventStorageEngine storageEngine(@Qualifier("axonMongoClient") MongoClient client, Serializer serializer) {
        return MongoEventStorageEngine
                .builder()
                .mongoTemplate(DefaultMongoTemplate.builder().mongoDatabase(client, POS_MONGO_DB).build())
                .eventSerializer(serializer)
                .build();
    }

    @Bean
    public EmbeddedEventStore eventStore(EventStorageEngine storageEngine, AxonConfiguration configuration) {
        return EmbeddedEventStore.builder()
                .storageEngine(storageEngine)
                .messageMonitor(configuration.messageMonitor(EventStore.class, "eventStore"))
                .build();
    }

    public void loadEnvironments() {
        log.info("Load init variables");
        this.POS_MONGO_HOST = System.getenv("POS_MONGO_HOST") == null ? "" : System.getenv("POS_MONGO_HOST");
        this.POS_MONGO_PORT = System.getenv("POS_MONGO_PORT") == null ? "" : System.getenv("POS_MONGO_PORT");
        this.POS_MONGO_DB = System.getenv("POS_MONGO_DB") == null ? "" : System.getenv("POS_MONGO_DB");
        this.POS_MONGO_USERNAME = System.getenv("POS_MONGO_USERNAME") == null ? "" : System.getenv("POS_MONGO_USERNAME");
        this.POS_MONGO_PASSWORD = System.getenv("POS_MONGO_PASSWORD") == null ? "" : System.getenv("POS_MONGO_PASSWORD");
        log.info("Load fin variables");
    }

}
