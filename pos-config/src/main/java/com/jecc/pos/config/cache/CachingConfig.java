package com.jecc.pos.config.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

@EnableCaching
@Configuration
@Slf4j
public class CachingConfig extends CachingConfigurerSupport {

    @Value("${spring.redis.defaultTTL:5}")
    private String defaultTTL;

    @Bean
    @Primary
    public RedisCacheManager customCacheManager(RedisConnectionFactory connectionFactory) {
        log.info("=================================================================");
        log.info("redis custom cacheManager : {} ", defaultTTL);
        log.info("=================================================================");

        long vdefaultTTL = Long.parseLong(defaultTTL);
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig().disableCachingNullValues();
        config = config
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(RedisSerializer.java()))
                //.prefixCacheNameWith(this.getClass().getPackageName() + ".")
                .entryTtl(Duration.ofMinutes(vdefaultTTL));

        return RedisCacheManager.builder(connectionFactory)
                .cacheDefaults(config)
                .build();
    }

}
