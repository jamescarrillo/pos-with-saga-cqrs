package com.jecc.pos.config.middleware;

import an.awesome.pipelinr.Command;

public class LoggableMiddleware implements Command.Middleware {

    @Override
    public <R, C extends Command<R>> R invoke(C command, Next<R> next) {
        return next.invoke();
    }
}
