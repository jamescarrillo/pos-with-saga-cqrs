package com.jecc.pos.config.kafka;

import com.jecc.pos.util.others.Event;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Configuration
public class KafkaProducer {

    private String bootstrapAddress; //"localhost:9092";

    public KafkaProducer() {
        this.bootstrapAddress = System.getenv("POS_KAFKA_HOST") == null ? "localhost" : System.getenv("POS_KAFKA_HOST");
        this.bootstrapAddress += ":" + (System.getenv("POS_KAFKA_PORT") == null ? "9092" : System.getenv("POS_KAFKA_PORT"));
        log.info("POS_KAFKA_HOST {}", System.getenv("POS_KAFKA_HOST"));
        log.info("POS_KAFKA_PORT {}", System.getenv("POS_KAFKA_PORT"));
        log.info("KafkaProducer {}", this.bootstrapAddress);
    }

    @Bean
    public ProducerFactory<String, Event<?>> producerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapAddress);
        configProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        configProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaTemplate<String, Event<?>> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

}
