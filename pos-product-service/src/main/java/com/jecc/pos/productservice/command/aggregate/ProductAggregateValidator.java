package com.jecc.pos.productservice.command.aggregate;

import com.jecc.pos.commons.commands.product.create.CreateProductCommand;
import com.jecc.pos.commons.commands.product.delete.DeleteProductCommand;
import com.jecc.pos.commons.commands.product.update.UpdateProductCommand;
import com.jecc.pos.entities.ProductEntity;
import com.jecc.pos.productservice.repository.IProductRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class ProductAggregateValidator {

    private final IProductRepository productRepository;

    @Autowired
    public ProductAggregateValidator(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void create(CreateProductCommand createProductCommand) {
        log.info("Validator createProductCommand init");
        Optional<ProductEntity> optionalProductEntity =
                this.productRepository.findByCodeOrName(
                        createProductCommand.getCode(),
                        createProductCommand.getName(),
                        ConstantsLibrary.STATUS_CREATED);
        if (optionalProductEntity.isPresent()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_ALREADY_EXIST_CODE, null, new String[]{ErrorsConstants.PRODUCT_ALREADY_EXIST_MESSAGE});
        }
        log.info("Validator createProductCommand fin");
    }

    public void update(UpdateProductCommand updateProductCommand) {
        log.info("Validator updateProductCommand init");
        //validation exist
        Optional<ProductEntity> optionalProductEntityValidateExist = this.productRepository.findByProductIdAndStatus(
                updateProductCommand.getProductId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalProductEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.PRODUCT_NOT_FOUND_MESSAGE});
        }
        Optional<ProductEntity> optionalProductEntityValidate =
                this.productRepository.findByCodeOrName(
                        updateProductCommand.getCode(),
                        updateProductCommand.getName(),
                        updateProductCommand.getProductId(),
                        ConstantsLibrary.STATUS_CREATED);
        if (optionalProductEntityValidate.isPresent()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_ALREADY_EXIST_CODE, null, new String[]{ErrorsConstants.PRODUCT_ALREADY_EXIST_MESSAGE});
        }
        log.info("Validator updateProductCommand fin");
    }

    public void delete(DeleteProductCommand deleteProductCommand) {
        log.info("Validator deleteProductCommand init");
        //validation exist
        Optional<ProductEntity> optionalProductEntityValidateExist = this.productRepository.findByProductIdAndStatus(
                deleteProductCommand.getProductId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalProductEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.PRODUCT_NOT_FOUND_MESSAGE});
        }
        log.info("Validator deleteProductCommand fin");
    }

}
