package com.jecc.pos.productservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.querys.product.ProductByIdQueryProjection;
import com.jecc.pos.commons.querys.product.ProductPaginationQueryProjection;
import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsPagination;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("product")
@Log4j2
public class ProductQueryRESTController {

    @Resource
    private Pipeline pipeline;

    @GetMapping
    public ResponseEntity<ApiResponse<Pagination<ProductDTO>>> getPagination(
            @RequestParam(defaultValue = ConstantsPagination.DEFAULT_FILTER) String filter,
            @RequestParam(defaultValue = ConstantsPagination.PAGE) int page,
            @RequestParam(defaultValue = ConstantsPagination.SIZE) int size) {
        ProductPaginationQueryProjection familyPaginationQueryProjection = new ProductPaginationQueryProjection();
        familyPaginationQueryProjection.setFilter(filter);
        familyPaginationQueryProjection.setPage(page);
        familyPaginationQueryProjection.setSize(size);
        return new ResponseEntity<>(pipeline.send(familyPaginationQueryProjection), HttpStatus.ACCEPTED);
    }

    @GetMapping("{id}")
    public ResponseEntity<ApiResponse<ProductDTO>> getById(@PathVariable String id) {
        return new ResponseEntity<>(pipeline.send(new ProductByIdQueryProjection(id)), HttpStatus.ACCEPTED);
    }

}
