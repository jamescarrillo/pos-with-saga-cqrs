package com.jecc.pos.productservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.product.ProductByIdQueryProjection;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.entities.ProductEntity;
import com.jecc.pos.productservice.repository.IProductRepository;
import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProductByIdQueryHandler implements Command.Handler<ProductByIdQueryProjection, ApiResponse<ProductDTO>> {

    private final IProductRepository productRepository;

    @Autowired
    public ProductByIdQueryHandler(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public ApiResponse<ProductDTO> handle(ProductByIdQueryProjection productByIdQueryProjection) {
        Optional<ProductEntity> optionalProductEntity = this.productRepository.findByProductIdAndStatus(
                productByIdQueryProjection.getProductId(), ConstantsLibrary.STATUS_CREATED);
        /*
        ProductDTO productDTO = null;
        if (optionalProductEntity.isPresent()) {
            productDTO = ObjectMapperUtils.map(optionalProductEntity.get(), ProductDTO.class);
        }
         */
        optionalProductEntity.orElseThrow(() ->
                new CustomErrorException(
                        HttpStatus.NOT_FOUND,
                        ErrorsConstants.PRODUCT_NOT_FOUND_CODE,
                        new String[]{ErrorsConstants.PRODUCT_NOT_FOUND_MESSAGE},
                        //List.of(ErrorsConstants.PRODUCT_NOT_FOUND_MESSAGE).toArray(),
                        ErrorsConstants.PRODUCT_NOT_FOUND_CODE));
        return new ApiResponse.Builder<ProductDTO>()
                .builSuccesful(optionalProductEntity.get().getDTO())
                .build();
    }
}
