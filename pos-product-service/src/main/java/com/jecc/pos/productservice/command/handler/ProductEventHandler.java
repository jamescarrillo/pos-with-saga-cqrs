package com.jecc.pos.productservice.command.handler;

import com.jecc.pos.commons.events.product.CreateProductEvent;
import com.jecc.pos.commons.events.product.DeleteProductEvent;
import com.jecc.pos.commons.events.product.UpdateProductEvent;
import com.jecc.pos.entities.ProductEntity;
import com.jecc.pos.productservice.repository.IProductRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class ProductEventHandler {

    private final IProductRepository productRepository;

    @Autowired
    public ProductEventHandler(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @EventHandler
    public void on(CreateProductEvent createProductEvent) {
        log.info("Handling createProductEvent start...");
        ProductEntity productEntity = new ProductEntity();
        BeanUtils.copyProperties(createProductEvent, productEntity);
        this.productRepository.save(productEntity);
        log.info("Handling createProductEvent end...");
    }

    @EventHandler
    public void on(UpdateProductEvent updateProductEvent) {
        log.info("Handling updateProductEvent start...");
        Optional<ProductEntity> optionalProductEntity = this.productRepository.findByProductIdAndStatus(
                updateProductEvent.getProductId(),
                ConstantsLibrary.STATUS_CREATED);
        if (optionalProductEntity.isPresent()) {
            ProductEntity productEntity = optionalProductEntity.get();
            productEntity.setCode(updateProductEvent.getCode());
            productEntity.setName(updateProductEvent.getName());
            productEntity.setUpdateAt(updateProductEvent.getUpdateAt());
            productEntity.setUserUpdate(updateProductEvent.getUserUpdate());
            this.productRepository.save(productEntity);
        }
        log.info("Handling updateProductEvent end...");
    }

    @EventHandler
    public void on(DeleteProductEvent deleteProductEvent) {
        log.info("Handling deleteProductEvent start...");
        Optional<ProductEntity> optionalProductEntity = this.productRepository.findByProductIdAndStatus(
                deleteProductEvent.getProductId(),
                ConstantsLibrary.STATUS_CREATED);
        if (optionalProductEntity.isPresent()) {
            ProductEntity productEntity = optionalProductEntity.get();
            productEntity.setStatus(deleteProductEvent.getStatus());
            productEntity.setDeleteAt(deleteProductEvent.getDeleteAt());
            productEntity.setUserDelete(deleteProductEvent.getUserDelete());
            this.productRepository.save(productEntity);
        }
        log.info("Handling deleteProductEvent end...");
    }


}
