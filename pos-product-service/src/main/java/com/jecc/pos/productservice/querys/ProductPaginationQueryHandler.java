package com.jecc.pos.productservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.product.ProductPaginationQueryProjection;
import com.jecc.pos.entities.ProductEntity;
import com.jecc.pos.productservice.repository.IProductRepository;
import com.jecc.pos.types.ProductDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Log4j2
public class ProductPaginationQueryHandler implements Command.Handler<ProductPaginationQueryProjection, ApiResponse<Pagination<ProductDTO>>> {

    private final IProductRepository productRepository;

    @Autowired
    public ProductPaginationQueryHandler(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Cacheable(value = "value", keyGenerator = "customKeyGenerator", cacheManager = "customCacheManager")
    @Override
    public ApiResponse<Pagination<ProductDTO>> handle(ProductPaginationQueryProjection productPaginationQueryProjection) {
        log.info("ProductPaginationQueryHandler init");
        Pagination<ProductDTO> pagination = new Pagination<>();
        Pageable pageable = PageRequest.of(productPaginationQueryProjection.getPage(), productPaginationQueryProjection.getSize());
        pagination.setCountFilter(
                this.productRepository.findCountEntities(ConstantsLibrary.STATUS_CREATED,
                        productPaginationQueryProjection.getFilter())
        );
        pagination.setTotalPages(pagination.processAndGetTotalPages(productPaginationQueryProjection.getSize()));
        if (pagination.getCountFilter() > 0) {
            Optional<List<ProductEntity>> optionalProductEntities = this.productRepository.findEntities(
                    ConstantsLibrary.STATUS_CREATED,
                    productPaginationQueryProjection.getFilter(),
                    pageable);
            pagination.setList(optionalProductEntities.orElse(new ArrayList<>()).stream().map(ProductEntity::getDTO).collect(Collectors.toList()));
        }
        log.info("ProductPaginationQueryHandler fin");
        return new ApiResponse.Builder<Pagination<ProductDTO>>()
                .builSuccesful(pagination)
                .build();
    }

}
