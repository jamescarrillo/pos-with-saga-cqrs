package com.jecc.pos.productservice;

import org.axonframework.springboot.autoconfig.JdbcAutoConfiguration;
import org.axonframework.springboot.autoconfig.JpaAutoConfiguration;
import org.axonframework.springboot.autoconfig.JpaEventStoreAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
        scanBasePackages = {
                "com.jecc.pos.config",
                "com.jecc.pos.entities",
                "com.jecc.pos.commons",
                "com.jecc.pos.productservice"
        },
        exclude = {
                JpaEventStoreAutoConfiguration.class,
                JpaAutoConfiguration.class,
                JdbcAutoConfiguration.class
        }
)
@EntityScan("com.jecc.pos.entities")
@EnableJpaRepositories("com.jecc.pos.productservice.repository")
public class PosProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PosProductServiceApplication.class, args);
    }

}
