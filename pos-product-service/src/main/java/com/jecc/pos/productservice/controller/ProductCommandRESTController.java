package com.jecc.pos.productservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.commands.product.create.CreateProductCommand;
import com.jecc.pos.commons.commands.product.delete.DeleteProductCommand;
import com.jecc.pos.commons.commands.product.update.UpdateProductCommand;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("product")
@Log4j2
public class ProductCommandRESTController {

    @Resource
    private Pipeline pipeline;

    @PostMapping
    public ResponseEntity<ApiResponse<CreateProductCommand>> create(@RequestBody CreateProductCommand createProductCommand) {
        return new ResponseEntity<>(this.pipeline.send(createProductCommand), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ApiResponse<UpdateProductCommand>> update(@RequestBody UpdateProductCommand updateProductCommand) {
        return new ResponseEntity<>(this.pipeline.send(updateProductCommand), HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<ApiResponse<DeleteProductCommand>> delete(@RequestBody DeleteProductCommand deleteProductCommand) {
        return new ResponseEntity<>(this.pipeline.send(deleteProductCommand), HttpStatus.CREATED);
    }


}
