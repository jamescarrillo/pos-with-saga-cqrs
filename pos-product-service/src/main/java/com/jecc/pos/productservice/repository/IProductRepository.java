package com.jecc.pos.productservice.repository;

import com.jecc.pos.entities.ProductEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IProductRepository extends JpaRepository<ProductEntity, String> {

    Optional<ProductEntity> findByProductIdAndStatus(String productId, String status);

    @Query(
            value = "select c from ProductEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%') or lower(c.name) like concat('%',:filter, '%') ) " +
                    "order by c.name "
    )
    Optional<List<ProductEntity>> findEntities(
            @Param("status") String status,
            @Param("filter") String filter,
            Pageable pageable);

    @Query(
            value = "select count(c) from ProductEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%') or lower(c.name) like concat('%',:filter, '%') ) "
    )
    Long findCountEntities(
            @Param("status") String status,
            @Param("filter") String filter);

    @Query(
            value = "select c from ProductEntity c " +
                    "where c.status = :status and (c.code = :code or c.name = :name) ")
    Optional<ProductEntity> findByCodeOrName(@Param("code") String code, @Param("name") String name, @Param("status") String status);


    @Query(
            value = "select c from ProductEntity c " +
                    "where c.status = :status and (c.code = :code or c.name = :name) and c.productId <> :productId")
    Optional<ProductEntity> findByCodeOrName(@Param("code") String code, @Param("name") String name, @Param("productId") String productId, @Param("status") String status);

}
