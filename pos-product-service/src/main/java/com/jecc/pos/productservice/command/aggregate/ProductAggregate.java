package com.jecc.pos.productservice.command.aggregate;

import com.jecc.pos.commons.commands.product.create.CreateProductCommand;
import com.jecc.pos.commons.commands.product.delete.DeleteProductCommand;
import com.jecc.pos.commons.commands.product.update.UpdateProductCommand;
import com.jecc.pos.commons.events.product.CreateProductEvent;
import com.jecc.pos.commons.events.product.DeleteProductEvent;
import com.jecc.pos.commons.events.product.UpdateProductEvent;
import com.jecc.pos.productservice.event.ProductEventProducer;
import com.jecc.pos.types.ProductDTO;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Aggregate
@Slf4j
public class ProductAggregate extends ProductDTO {

    @AggregateIdentifier
    private String productId;

    public ProductAggregate() {
    }

    @CommandHandler
    public ProductAggregate(CreateProductCommand createProductCommand,
                            @Autowired ProductAggregateValidator productAggregateValidator,
                            @Autowired ProductEventProducer productEventProducer) {
        log.info("createProductCommand received.");
        productAggregateValidator.create(createProductCommand);
        CreateProductEvent createProductEvent = new CreateProductEvent();
        BeanUtils.copyProperties(createProductCommand, createProductEvent);
        AggregateLifecycle.apply(createProductEvent);
        productEventProducer.publish(createProductEvent);
    }

    @CommandHandler
    public void on(UpdateProductCommand updateProductCommand,
                   @Autowired ProductAggregateValidator productAggregateValidator) {
        log.info("updateProductCommand received.");
        productAggregateValidator.update(updateProductCommand);
        UpdateProductEvent updateProductEvent = new UpdateProductEvent();
        BeanUtils.copyProperties(updateProductCommand, updateProductEvent);
        AggregateLifecycle.apply(updateProductEvent);
    }

    @CommandHandler
    public void on(DeleteProductCommand deleteProductCommand,
                   @Autowired ProductAggregateValidator productAggregateValidator) {
        log.info("deleteProductCommand received.");
        productAggregateValidator.delete(deleteProductCommand);
        DeleteProductEvent deleteProductEvent = new DeleteProductEvent();
        BeanUtils.copyProperties(deleteProductCommand, deleteProductCommand);
        AggregateLifecycle.apply(deleteProductEvent);
    }

    @EventSourcingHandler
    public void on(CreateProductEvent createProductEvent) {
        log.info("createProductEvent occurred.");
        this.productId = createProductEvent.getProductId();
        this.setCode(createProductEvent.getCode());
        this.setName(createProductEvent.getName());
        this.setStatus(createProductEvent.getStatus());
        this.setCreateAt(createProductEvent.getCreateAt());
        this.setUpdateAt(createProductEvent.getUpdateAt());
        this.setDeleteAt(createProductEvent.getDeleteAt());
        this.setUserCreate(createProductEvent.getUserCreate());
        this.setUserUpdate(createProductEvent.getUserUpdate());
        this.setUserDelete(createProductEvent.getUserDelete());
    }

    @EventSourcingHandler
    public void on(UpdateProductEvent updateProductEvent) {
        log.info("updateProductEvent occurred.");
        this.productId = updateProductEvent.getProductId();
        this.setCode(updateProductEvent.getCode());
        this.setName(updateProductEvent.getName());
        this.setUpdateAt(updateProductEvent.getUpdateAt());
        this.setUserUpdate(updateProductEvent.getUserUpdate());
    }

    @EventSourcingHandler
    public void on(DeleteProductEvent deleteProductEvent) {
        log.info("deleteProductEvent occurred.");
        this.productId = deleteProductEvent.getProductId();
        this.setStatus(deleteProductEvent.getStatus());
        this.setDeleteAt(deleteProductEvent.getDeleteAt());
        this.setUserDelete(deleteProductEvent.getUserDelete());
    }

}
