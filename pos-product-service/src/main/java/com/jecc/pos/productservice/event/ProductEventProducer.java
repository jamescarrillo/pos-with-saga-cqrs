package com.jecc.pos.productservice.event;

import com.jecc.pos.commons.events.product.CreateProductEvent;
import com.jecc.pos.commons.kafka.KafkaManagerProducer;
import com.jecc.pos.util.others.Event;
import com.jecc.pos.util.others.EventType;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
@Log4j2
public class ProductEventProducer {

    private final KafkaManagerProducer kafkaManagerProducer;

    @Value("${topic.customer.name:products}")
    private String topicProduct;

    @Autowired
    public ProductEventProducer(KafkaManagerProducer kafkaManagerProducer) {
        this.kafkaManagerProducer = kafkaManagerProducer;
    }

    public void publish(CreateProductEvent createProductEvent) {
        Event<CreateProductEvent> event = new Event<>();
        event.setId(UUID.randomUUID().toString());
        event.setType(EventType.CREATED);
        event.setDate(new Date());
        event.setData(createProductEvent);
        this.kafkaManagerProducer.publish(topicProduct, event);
    }


}
