package com.jecc.pos.orderservice.repository;

import com.jecc.pos.entities.OrderEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IOrderRepository extends JpaRepository<OrderEntity, String> {

    Optional<OrderEntity> findByOrderIdAndStatus(String orderId, String status);

    @Query(
            value = "select c from OrderEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%') or lower(c.customerName) like concat('%',:filter, '%') ) " +
                    "order by c.createAt desc "
    )
    Optional<List<OrderEntity>> findEntities(
            @Param("status") String status,
            @Param("filter") String filter,
            Pageable pageable);

    @Query(
            value = "select count(c) from OrderEntity c " +
                    "where c.status = :status " +
                    "and (lower(c.code) like concat('%',:filter,'%') or lower(c.customerName) like concat('%',:filter, '%') ) "
    )
    Long findCountEntities(
            @Param("status") String status,
            @Param("filter") String filter);

}
