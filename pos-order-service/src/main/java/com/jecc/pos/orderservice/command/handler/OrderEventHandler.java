package com.jecc.pos.orderservice.command.handler;

import com.jecc.pos.commons.events.order.CancelOrderEvent;
import com.jecc.pos.commons.events.order.CompleteOrderEvent;
import com.jecc.pos.commons.events.order.CreateOrderEvent;
import com.jecc.pos.entities.OrderEntity;
import com.jecc.pos.orderservice.repository.IOrderRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
@Slf4j
public class OrderEventHandler {

    private final IOrderRepository orderRepository;

    @Autowired
    public OrderEventHandler(IOrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @EventHandler
    public void on(CreateOrderEvent createOrderEvent) {
        log.info("Handling createOrderEvent start...");
        OrderEntity orderEntity = new OrderEntity();
        BeanUtils.copyProperties(createOrderEvent, orderEntity);
        this.orderRepository.save(orderEntity);
        log.info("Handling createOrderEvent end...");
    }

    @EventHandler
    public void on(CompleteOrderEvent completeOrderEvent) {
        log.info("Handling completeOrderEvent start...");
        changeOrderStatus(completeOrderEvent.getOrderId(), completeOrderEvent.getOrderStatus(), completeOrderEvent.getUpdateAt(), completeOrderEvent.getUserUpdate());
        log.info("Handling completeOrderEvent end...");
    }

    @EventHandler
    public void on(CancelOrderEvent cancelOrderEvent) {
        log.info("Handling cancelOrderEvent start...");
        changeOrderStatus(cancelOrderEvent.getOrderId(), cancelOrderEvent.getOrderStatus(), cancelOrderEvent.getUpdateAt(), cancelOrderEvent.getUserUpdate());
        log.info("Handling cancelOrderEvent end...");
    }

    private void changeOrderStatus(String orderId, String orderStatus, LocalDateTime updateAt, String userUpdate) {
        Optional<OrderEntity> optionalOrderEntity = this.orderRepository.findByOrderIdAndStatus(orderId,
                ConstantsLibrary.STATUS_CREATED);
        if (optionalOrderEntity.isPresent()) {
            OrderEntity orderEntity = optionalOrderEntity.get();
            orderEntity.setOrderStatus(orderStatus);
            orderEntity.setUpdateAt(updateAt);
            orderEntity.setUserUpdate(userUpdate);
            this.orderRepository.save(orderEntity);
        }
    }

}
