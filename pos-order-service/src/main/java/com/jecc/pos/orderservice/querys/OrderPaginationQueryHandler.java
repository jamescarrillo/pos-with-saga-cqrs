package com.jecc.pos.orderservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.order.OrderPaginationQueryProjection;
import com.jecc.pos.entities.OrderEntity;
import com.jecc.pos.orderservice.repository.IOrderRepository;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Log4j2
public class OrderPaginationQueryHandler implements Command.Handler<OrderPaginationQueryProjection, ApiResponse<Pagination<OrderDTO>>> {

    private final IOrderRepository orderRepository;

    @Autowired
    public OrderPaginationQueryHandler(IOrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Cacheable(value = "value", keyGenerator = "customKeyGenerator", cacheManager = "customCacheManager")
    @Override
    public ApiResponse<Pagination<OrderDTO>> handle(OrderPaginationQueryProjection orderPaginationQueryProjection) {
        log.info("OrderPaginationQueryHandler init");
        Pagination<OrderDTO> pagination = new Pagination<>();
        Pageable pageable = PageRequest.of(orderPaginationQueryProjection.getPage(), orderPaginationQueryProjection.getSize());
        pagination.setCountFilter(
                this.orderRepository.findCountEntities(ConstantsLibrary.STATUS_CREATED,
                        orderPaginationQueryProjection.getFilter())
        );
        pagination.setTotalPages(pagination.processAndGetTotalPages(orderPaginationQueryProjection.getSize()));
        if (pagination.getCountFilter() > 0) {
            Optional<List<OrderEntity>> optionalOrderEntities = this.orderRepository.findEntities(
                    ConstantsLibrary.STATUS_CREATED,
                    orderPaginationQueryProjection.getFilter(),
                    pageable);
            pagination.setList(optionalOrderEntities.orElse(new ArrayList<>()).stream().map(OrderEntity::getDTO).collect(Collectors.toList()));
        }
        log.info("OrderPaginationQueryHandler fin");
        return new ApiResponse.Builder<Pagination<OrderDTO>>()
                .builSuccesful(pagination)
                .build();
    }

}
