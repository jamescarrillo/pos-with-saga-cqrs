package com.jecc.pos.orderservice.command.handler;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.commands.order.cancel.CancelOrderCommand;
import com.jecc.pos.commons.commands.order.complete.CompleteOrderCommand;
import com.jecc.pos.commons.commands.payment.cancel.CancelPaymentCommand;
import com.jecc.pos.commons.commands.payment.create.CreatePaymentCommand;
import com.jecc.pos.commons.commands.shipment.create.CreateShipmentCommand;
import com.jecc.pos.commons.events.order.CancelOrderEvent;
import com.jecc.pos.commons.events.order.CompleteOrderEvent;
import com.jecc.pos.commons.events.order.CreateOrderEvent;
import com.jecc.pos.commons.events.payment.CancelPaymentEvent;
import com.jecc.pos.commons.events.payment.CreatePaymentEvent;
import com.jecc.pos.commons.events.shipment.CreateShipmentEvent;
import com.jecc.pos.orderservice.Default;
import lombok.extern.log4j.Log4j2;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;

import javax.annotation.Resource;

@Log4j2
@Saga
public class OrderProcessingSaga {

    @Resource
    private Pipeline pipeline;

    @StartSaga
    @SagaEventHandler(associationProperty = "orderId")
    private void handle(CreateOrderEvent createOrderEvent) {
        log.info("Start createOrderEvent in Saga for Order Id : {}",
                createOrderEvent.getOrderId());

        try {

            //validation data orderObject


            //validation data orderObject success, then
            CreatePaymentCommand createPaymentCommand = new CreatePaymentCommand();
            createPaymentCommand.setOrderId(createOrderEvent.getOrderId());
            createPaymentCommand.setUserCreate(Default.USER_CREATE);
            this.pipeline.send(createPaymentCommand);

        } catch (Exception e) {
            cancelOrderCommand(createOrderEvent.getOrderId());
        }

        log.info("End createOrderEvent in Saga for Order Id : {}",
                createOrderEvent.getOrderId());
    }

    @SagaEventHandler(associationProperty = "orderId")
    private void handle(CreatePaymentEvent createPaymentEvent) {
        log.info("Start createPaymentEvent in Saga for Order Id : {}",
                createPaymentEvent.getOrderId());
        try {
            CreateShipmentCommand createShipmentCommand = new CreateShipmentCommand();
            createShipmentCommand.setOrderId(createPaymentEvent.getOrderId());
            createShipmentCommand.setUserCreate(Default.USER_CREATE);
            this.pipeline.send(createShipmentCommand);
        } catch (Exception e) {
            cancelPaymentCommand(createPaymentEvent);
        }
        log.info("End createPaymentEvent in Saga for Order Id : {}",
                createPaymentEvent.getOrderId());
    }

    @SagaEventHandler(associationProperty = "orderId")
    private void handle(CreateShipmentEvent createShipmentEvent) {
        log.info("Start createShipmentEvent in Saga for Order Id : {}",
                createShipmentEvent.getOrderId());
        CompleteOrderCommand completeOrderCommand = new CompleteOrderCommand();
        completeOrderCommand.setOrderId(createShipmentEvent.getOrderId());
        completeOrderCommand.setOrderStatus("COMPLETED");
        completeOrderCommand.setUserUpdate(Default.USER_UPDATE);
        this.pipeline.send(completeOrderCommand);
        log.info("End createShipmentEvent in Saga for Order Id : {}",
                createShipmentEvent.getOrderId());
    }

    @SagaEventHandler(associationProperty = "orderId")
    @EndSaga
    public void handle(CompleteOrderEvent completeOrderEvent) {
        log.info("completeOrderEvent in Saga for Order Id : {}",
                completeOrderEvent.getOrderId());
    }

    @SagaEventHandler(associationProperty = "orderId")
    @EndSaga
    public void handle(CancelOrderEvent cancelOrderEvent) {
        log.info("cancelOrderEvent in Saga for Order Id : {}",
                cancelOrderEvent.getOrderId());
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(CancelPaymentEvent cancelPaymentEvent) {
        log.info("cancelOrderEvent in Saga for Order Id : {}",
                cancelPaymentEvent.getOrderId());
    }

    /*
        Cancel commands
     */

    private void cancelPaymentCommand(CreatePaymentEvent createPaymentEvent) {
        CancelPaymentCommand cancelPaymentCommand = new CancelPaymentCommand();
        cancelPaymentCommand.setOrderId(createPaymentEvent.getOrderId());
        cancelPaymentCommand.setOrderId(createPaymentEvent.getOrderId());
        cancelPaymentCommand.setPaymentStatus("CANCELLED");
        cancelPaymentCommand.setUserUpdate(Default.USER_UPDATE);
        this.pipeline.send(cancelPaymentCommand);
    }

    private void cancelOrderCommand(String orderId) {
        CancelOrderCommand cancelOrderCommand = new CancelOrderCommand();
        cancelOrderCommand.setOrderId(orderId);
        cancelOrderCommand.setOrderStatus("CANCELLED");
        this.pipeline.send(cancelOrderCommand);
    }

}
