package com.jecc.pos.orderservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.commands.order.cancel.CancelOrderCommand;
import com.jecc.pos.commons.commands.order.create.CreateOrderCommand;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("order")
@Log4j2
public class OrderCommandRESTController {

    @Resource
    private Pipeline pipeline;

    @PostMapping
    public ResponseEntity<ApiResponse<CreateOrderCommand>> create(@RequestBody CreateOrderCommand createOrderCommand) {
        return new ResponseEntity<>(this.pipeline.send(createOrderCommand), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ApiResponse<CancelOrderCommand>> update(@RequestBody CancelOrderCommand cancelOrderCommand) {
        return new ResponseEntity<>(this.pipeline.send(cancelOrderCommand), HttpStatus.CREATED);
    }


}
