package com.jecc.pos.orderservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.querys.order.OrderByIdQueryProjection;
import com.jecc.pos.commons.querys.order.OrderPaginationQueryProjection;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsPagination;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("order")
@Log4j2
public class OrderQueryRESTController {

    @Resource
    private Pipeline pipeline;

    @GetMapping
    public ResponseEntity<ApiResponse<Pagination<OrderDTO>>> getPagination(
            @RequestParam(defaultValue = ConstantsPagination.DEFAULT_FILTER) String filter,
            @RequestParam(defaultValue = ConstantsPagination.PAGE) int page,
            @RequestParam(defaultValue = ConstantsPagination.SIZE) int size) {
        OrderPaginationQueryProjection orderPaginationQueryProjection = new OrderPaginationQueryProjection();
        orderPaginationQueryProjection.setFilter(filter);
        orderPaginationQueryProjection.setPage(page);
        orderPaginationQueryProjection.setSize(size);
        return new ResponseEntity<>(this.pipeline.send(orderPaginationQueryProjection), HttpStatus.ACCEPTED);
    }

    @GetMapping("{id}")
    public ResponseEntity<ApiResponse<OrderDTO>> getById(@PathVariable String id) {
        return new ResponseEntity<>(this.pipeline.send(new OrderByIdQueryProjection(id)), HttpStatus.ACCEPTED);
    }

}
