package com.jecc.pos.orderservice.command.aggregate;

import com.jecc.pos.commons.commands.order.cancel.CancelOrderCommand;
import com.jecc.pos.commons.commands.order.complete.CompleteOrderCommand;
import com.jecc.pos.commons.commands.order.create.CreateOrderCommand;
import com.jecc.pos.entities.OrderEntity;
import com.jecc.pos.orderservice.repository.IOrderRepository;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class OrderAggregateValidator {

    private final IOrderRepository orderRepository;

    @Autowired
    public OrderAggregateValidator(IOrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void create(CreateOrderCommand createOrderCommand) {
        log.info("Validator createOrderCommand init");
        log.info("Valid {}", createOrderCommand.getCode());
        log.info("Validator createOrderCommand fin");
    }

    public void cancel(CancelOrderCommand cancelOrderCommand) {
        log.info("Validator cancelOrderCommand init");
        //validation exist
        Optional<OrderEntity> optionalOrderEntityValidateExist = this.orderRepository.findByOrderIdAndStatus(cancelOrderCommand.getOrderId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalOrderEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.ORDER_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.ORDER_NOT_FOUND_MESSAGE});
        }
        log.info("Validator cancelOrderCommand fin");
    }

    public void complete(CompleteOrderCommand completeOrderCommand) {
        log.info("Validator completeOrderCommand init");
        //validation exist
        Optional<OrderEntity> optionalOrderEntityValidateExist = this.orderRepository.findByOrderIdAndStatus(completeOrderCommand.getOrderId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalOrderEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.ORDER_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.ORDER_NOT_FOUND_MESSAGE});
        }
        log.info("Validator completeOrderCommand fin");
    }

}
