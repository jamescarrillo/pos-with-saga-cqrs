package com.jecc.pos.orderservice.command.aggregate;

import com.jecc.pos.commons.commands.order.cancel.CancelOrderCommand;
import com.jecc.pos.commons.commands.order.complete.CompleteOrderCommand;
import com.jecc.pos.commons.commands.order.create.CreateOrderCommand;
import com.jecc.pos.commons.events.order.CancelOrderEvent;
import com.jecc.pos.commons.events.order.CompleteOrderEvent;
import com.jecc.pos.commons.events.order.CreateOrderEvent;
import com.jecc.pos.types.OrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Aggregate
@Slf4j
public class OrderAggregate extends OrderDTO {

    @AggregateIdentifier
    private String orderId;

    public OrderAggregate() {
    }

    @CommandHandler
    public OrderAggregate(CreateOrderCommand createOrderCommand,
                          @Autowired OrderAggregateValidator orderAggregateValidator) {
        log.info("createOrderCommand received.");
        orderAggregateValidator.create(createOrderCommand);
        CreateOrderEvent createOrderEvent = new CreateOrderEvent();
        BeanUtils.copyProperties(createOrderCommand, createOrderEvent);
        AggregateLifecycle.apply(createOrderEvent);
    }

    @CommandHandler
    public void on(CompleteOrderCommand completeOrderCommand,
                   @Autowired OrderAggregateValidator orderAggregateValidator) {
        log.info("cancelOrderCommand received.");
        orderAggregateValidator.complete(completeOrderCommand);
        CompleteOrderEvent completeOrderEvent = new CompleteOrderEvent();
        BeanUtils.copyProperties(completeOrderCommand, completeOrderEvent);
        AggregateLifecycle.apply(completeOrderEvent);
    }

    @CommandHandler
    public void on(CancelOrderCommand cancelOrderCommand,
                   @Autowired OrderAggregateValidator orderAggregateValidator) {
        log.info("cancelOrderCommand received.");
        orderAggregateValidator.cancel(cancelOrderCommand);
        CancelOrderEvent cancelOrderEvent = new CancelOrderEvent();
        BeanUtils.copyProperties(cancelOrderCommand, cancelOrderEvent);
        AggregateLifecycle.apply(cancelOrderEvent);
    }

    @EventSourcingHandler
    public void on(CreateOrderEvent createOrderEvent) {
        log.info("createOrderEvent occurred.");
        this.orderId = createOrderEvent.getOrderId();
        this.setCode(createOrderEvent.getCode());
        this.setOrderStatus(createOrderEvent.getOrderStatus());
        this.setProductId(createOrderEvent.getProductId());
        this.setQuantity(createOrderEvent.getQuantity());
        this.setStatus(createOrderEvent.getStatus());
        this.setCreateAt(createOrderEvent.getCreateAt());
        this.setUpdateAt(createOrderEvent.getUpdateAt());
        this.setDeleteAt(createOrderEvent.getDeleteAt());
        this.setUserCreate(createOrderEvent.getUserCreate());
        this.setUserUpdate(createOrderEvent.getUserUpdate());
        this.setUserDelete(createOrderEvent.getUserDelete());
    }

    @EventSourcingHandler
    public void on(CompleteOrderEvent completeOrderEvent) {
        log.info("completeOrderEvent occurred.");
        this.orderId = completeOrderEvent.getOrderId();
        this.setOrderStatus(completeOrderEvent.getOrderStatus());
        this.setUpdateAt(completeOrderEvent.getUpdateAt());
        this.setUserUpdate(completeOrderEvent.getUserUpdate());
    }

    @EventSourcingHandler
    public void on(CancelOrderEvent cancelOrderEvent) {
        log.info("cancelOrderEvent occurred.");
        this.orderId = cancelOrderEvent.getOrderId();
        this.setOrderStatus(cancelOrderEvent.getOrderStatus());
        this.setUpdateAt(cancelOrderEvent.getUpdateAt());
        this.setUserUpdate(cancelOrderEvent.getUserUpdate());
    }

}
