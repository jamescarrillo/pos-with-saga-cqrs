package com.jecc.pos.orderservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.commons.querys.order.OrderByIdQueryProjection;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.entities.OrderEntity;
import com.jecc.pos.orderservice.repository.IOrderRepository;
import com.jecc.pos.types.OrderDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderByIdQueryHandler implements Command.Handler<OrderByIdQueryProjection, ApiResponse<OrderDTO>> {

    private final IOrderRepository OrderRepository;

    @Autowired
    public OrderByIdQueryHandler(IOrderRepository OrderRepository) {
        this.OrderRepository = OrderRepository;
    }

    @Override
    public ApiResponse<OrderDTO> handle(OrderByIdQueryProjection OrderByIdQueryProjection) {
        Optional<OrderEntity> optionalOrderEntity = this.OrderRepository.findByOrderIdAndStatus(OrderByIdQueryProjection.getOrderId(), ConstantsLibrary.STATUS_CREATED);
        optionalOrderEntity.orElseThrow(() ->
                new CustomErrorException(
                        HttpStatus.NOT_FOUND,
                        ErrorsConstants.ORDER_NOT_FOUND_CODE,
                        new String[]{ErrorsConstants.ORDER_NOT_FOUND_MESSAGE},
                        ErrorsConstants.ORDER_NOT_FOUND_CODE));
        return new ApiResponse.Builder<OrderDTO>()
                .builSuccesful(optionalOrderEntity.get().getDTO())
                .build();
    }
}
