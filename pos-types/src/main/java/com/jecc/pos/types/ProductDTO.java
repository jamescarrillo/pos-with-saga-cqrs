package com.jecc.pos.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jecc.pos.util.auditory.AuditoryDTO;
import lombok.*;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDTO extends AuditoryDTO implements Serializable {

    private String productId;
    private String code;
    private String name;
    private Double stock;

}
