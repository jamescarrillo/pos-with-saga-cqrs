package com.jecc.pos.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jecc.pos.util.auditory.AuditoryDTO;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentDTO extends AuditoryDTO implements Serializable {

    private String paymentId;
    private String code;
    private String paymentStatus;
    private String orderId;
}
