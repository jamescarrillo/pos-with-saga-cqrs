package com.jecc.pos.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jecc.pos.util.auditory.AuditoryDTO;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipmentDTO extends AuditoryDTO implements Serializable {

    private String shipmentId;
    private String code;
    private String shipmentStatus;
    private String orderId;
}
