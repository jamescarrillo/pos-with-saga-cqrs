package com.jecc.pos.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jecc.pos.util.auditory.AuditoryDTO;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDTO extends AuditoryDTO implements Serializable {

    private String orderId;
    private String code;
    private String customerCode;
    private String customerName;
    private String productId;
    private String orderStatus;
    private Double quantity;

}
