package com.jecc.pos.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jecc.pos.util.auditory.AuditoryDTO;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InventoryDTO extends AuditoryDTO implements Serializable {

    private String inventoryId;
    private Double stock;
    private String productId;

}
