package com.jecc.pos.inventoryservice.command.handler;

import com.jecc.pos.inventoryservice.repository.IInventoryRepository;
import com.jecc.pos.commons.events.inventory.CreateInventoryEvent;
import com.jecc.pos.commons.events.inventory.DeleteInventoryEvent;
import com.jecc.pos.commons.events.inventory.UpdateInventoryEvent;
import com.jecc.pos.entities.InventoryEntity;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class InventoryEventHandler {

    private final IInventoryRepository inventoryRepository;

    @Autowired
    public InventoryEventHandler(IInventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    @EventHandler
    public void on(CreateInventoryEvent createInventoryEvent) {
        log.info("Handling createInventoryEvent start...");
        InventoryEntity inventoryEntity = new InventoryEntity();
        BeanUtils.copyProperties(createInventoryEvent, inventoryEntity);
        this.inventoryRepository.save(inventoryEntity);
        log.info("Handling createInventoryEvent end...");
    }

    @EventHandler
    public void on(UpdateInventoryEvent updateInventoryEvent) {
        log.info("Handling updateInventoryEvent start...");
        Optional<InventoryEntity> optionalInventoryEntity = this.inventoryRepository.findByInventoryIdAndStatus(
                updateInventoryEvent.getInventoryId(),
                ConstantsLibrary.STATUS_CREATED);
        if (optionalInventoryEntity.isPresent()) {
            InventoryEntity inventoryEntity = optionalInventoryEntity.get();
            inventoryEntity.setProductId(updateInventoryEvent.getProductId());
            inventoryEntity.setStock(updateInventoryEvent.getStock());
            inventoryEntity.setUpdateAt(updateInventoryEvent.getUpdateAt());
            inventoryEntity.setUserUpdate(updateInventoryEvent.getUserUpdate());
            this.inventoryRepository.save(inventoryEntity);
        }
        log.info("Handling updateInventoryEvent end...");
    }

    @EventHandler
    public void on(DeleteInventoryEvent deleteInventoryEvent) {
        log.info("Handling deleteInventoryEvent start...");
        Optional<InventoryEntity> optionalInventoryEntity = this.inventoryRepository.findByInventoryIdAndStatus(
                deleteInventoryEvent.getInventoryId(),
                ConstantsLibrary.STATUS_CREATED);
        if (optionalInventoryEntity.isPresent()) {
            InventoryEntity inventoryEntity = optionalInventoryEntity.get();
            inventoryEntity.setStatus(deleteInventoryEvent.getStatus());
            inventoryEntity.setDeleteAt(deleteInventoryEvent.getDeleteAt());
            inventoryEntity.setUserDelete(deleteInventoryEvent.getUserDelete());
            this.inventoryRepository.save(inventoryEntity);
        }
        log.info("Handling deleteInventoryEvent end...");
    }


}
