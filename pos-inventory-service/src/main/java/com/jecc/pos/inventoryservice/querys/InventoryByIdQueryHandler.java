package com.jecc.pos.inventoryservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.inventoryservice.repository.IInventoryRepository;
import com.jecc.pos.commons.querys.inventory.InventoryByIdQueryProjection;
import com.jecc.pos.config.exception.CustomErrorException;
import com.jecc.pos.entities.InventoryEntity;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InventoryByIdQueryHandler implements Command.Handler<InventoryByIdQueryProjection, ApiResponse<InventoryDTO>> {

    private final IInventoryRepository productRepository;

    @Autowired
    public InventoryByIdQueryHandler(IInventoryRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public ApiResponse<InventoryDTO> handle(InventoryByIdQueryProjection productByIdQueryProjection) {
        Optional<InventoryEntity> optionalInventoryEntity = this.productRepository.findByInventoryIdAndStatus(
                productByIdQueryProjection.getInventoryId(), ConstantsLibrary.STATUS_CREATED);
        optionalInventoryEntity.orElseThrow(() ->
                new CustomErrorException(
                        HttpStatus.NOT_FOUND,
                        ErrorsConstants.PRODUCT_NOT_FOUND_CODE,
                        new String[]{ErrorsConstants.PRODUCT_NOT_FOUND_MESSAGE},
                        ErrorsConstants.PRODUCT_NOT_FOUND_CODE));
        return new ApiResponse.Builder<InventoryDTO>()
                .builSuccesful(optionalInventoryEntity.get().getDTO())
                .build();
    }
}
