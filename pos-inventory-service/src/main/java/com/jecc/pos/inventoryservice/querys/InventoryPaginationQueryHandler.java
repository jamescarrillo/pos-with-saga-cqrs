package com.jecc.pos.inventoryservice.querys;

import an.awesome.pipelinr.Command;
import com.jecc.pos.inventoryservice.repository.IInventoryRepository;
import com.jecc.pos.commons.querys.inventory.InventoryPaginationQueryProjection;
import com.jecc.pos.entities.InventoryEntity;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsLibrary;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Log4j2
public class InventoryPaginationQueryHandler implements Command.Handler<InventoryPaginationQueryProjection, ApiResponse<Pagination<InventoryDTO>>> {

    private final IInventoryRepository inventoryRepository;

    @Autowired
    public InventoryPaginationQueryHandler(IInventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    @Cacheable(value = "value", keyGenerator = "customKeyGenerator", cacheManager = "customCacheManager")
    @Override
    public ApiResponse<Pagination<InventoryDTO>> handle(InventoryPaginationQueryProjection inventoryPaginationQueryProjection) {
        log.info("InventoryPaginationQueryHandler init");
        Pagination<InventoryDTO> pagination = new Pagination<>();
        pagination.setTotalPages(pagination.processAndGetTotalPages(inventoryPaginationQueryProjection.getSize()));
        Optional<List<InventoryEntity>> optionalProductEntities = this.inventoryRepository.findEntities(
                ConstantsLibrary.STATUS_CREATED,
                inventoryPaginationQueryProjection.getProductsIds());
        pagination.setList(optionalProductEntities.orElse(new ArrayList<>()).stream().map(InventoryEntity::getDTO).collect(Collectors.toList()));
        //pagination.setCountFilter(pagination.getList().size());
        //pagination.setTotalPages(pagination.processAndGetTotalPages(inventoryPaginationQueryProjection.getSize()));
        log.info("InventoryPaginationQueryHandler fin");
        return new ApiResponse.Builder<Pagination<InventoryDTO>>()
                .builSuccesful(pagination)
                .build();
    }

}
