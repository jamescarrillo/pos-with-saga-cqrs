package com.jecc.pos.inventoryservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.querys.inventory.InventoryByIdQueryProjection;
import com.jecc.pos.commons.querys.inventory.InventoryPaginationQueryProjection;
import com.jecc.pos.types.InventoryDTO;
import com.jecc.pos.util.api.ApiResponse;
import com.jecc.pos.util.api.Pagination;
import com.jecc.pos.util.constants.ConstantsPagination;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("inventory")
@Log4j2
public class InventoryQueryRESTController {

    @Resource
    private Pipeline pipeline;

    @GetMapping
    public ResponseEntity<ApiResponse<Pagination<InventoryDTO>>> getPagination(
            @RequestParam List<String> productsIds,
            @RequestParam(defaultValue = ConstantsPagination.PAGE) int page,
            @RequestParam(defaultValue = ConstantsPagination.SIZE) int size) {
        InventoryPaginationQueryProjection familyPaginationQueryProjection = new InventoryPaginationQueryProjection();
        familyPaginationQueryProjection.setProductsIds(productsIds);
        familyPaginationQueryProjection.setPage(page);
        familyPaginationQueryProjection.setSize(size);
        return new ResponseEntity<>(pipeline.send(familyPaginationQueryProjection), HttpStatus.ACCEPTED);
    }

    @GetMapping("{id}")
    public ResponseEntity<ApiResponse<InventoryDTO>> getById(@PathVariable String id) {
        return new ResponseEntity<>(pipeline.send(new InventoryByIdQueryProjection(id)), HttpStatus.ACCEPTED);
    }

}
