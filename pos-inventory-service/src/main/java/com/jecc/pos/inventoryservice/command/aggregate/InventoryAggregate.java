package com.jecc.pos.inventoryservice.command.aggregate;

import com.jecc.pos.commons.commands.inventory.create.CreateInventoryCommand;
import com.jecc.pos.commons.commands.inventory.delete.DeleteInventoryCommand;
import com.jecc.pos.commons.commands.inventory.update.UpdateInventoryCommand;
import com.jecc.pos.commons.events.inventory.CreateInventoryEvent;
import com.jecc.pos.commons.events.inventory.DeleteInventoryEvent;
import com.jecc.pos.commons.events.inventory.UpdateInventoryEvent;
import com.jecc.pos.types.InventoryDTO;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Aggregate
@Slf4j
public class InventoryAggregate extends InventoryDTO {

    @AggregateIdentifier
    private String inventoryId;

    public InventoryAggregate() {
    }

    @CommandHandler
    public InventoryAggregate(CreateInventoryCommand createInventoryCommand,
                              @Autowired InventoryAggregateValidator productAggregateValidator) {
        log.info("createInventoryCommand received.");
        productAggregateValidator.create(createInventoryCommand);
        CreateInventoryEvent createInventoryEvent = new CreateInventoryEvent();
        BeanUtils.copyProperties(createInventoryCommand, createInventoryEvent);
        AggregateLifecycle.apply(createInventoryEvent);
    }

    @CommandHandler
    public void on(UpdateInventoryCommand updateInventoryCommand,
                   @Autowired InventoryAggregateValidator productAggregateValidator) {
        log.info("updateInventoryCommand received.");
        productAggregateValidator.update(updateInventoryCommand);
        UpdateInventoryEvent updateInventoryEvent = new UpdateInventoryEvent();
        BeanUtils.copyProperties(updateInventoryCommand, updateInventoryEvent);
        AggregateLifecycle.apply(updateInventoryEvent);
    }

    @CommandHandler
    public void on(DeleteInventoryCommand deleteInventoryCommand,
                   @Autowired InventoryAggregateValidator productAggregateValidator) {
        log.info("deleteInventoryCommand received.");
        productAggregateValidator.delete(deleteInventoryCommand);
        DeleteInventoryEvent deleteInventoryEvent = new DeleteInventoryEvent();
        BeanUtils.copyProperties(deleteInventoryCommand, deleteInventoryCommand);
        AggregateLifecycle.apply(deleteInventoryEvent);
    }

    @EventSourcingHandler
    public void on(CreateInventoryEvent createInventoryEvent) {
        log.info("createInventoryEvent occurred.");
        this.inventoryId = createInventoryEvent.getInventoryId();
        this.setProductId(createInventoryEvent.getProductId());
        this.setStock(createInventoryEvent.getStock());
        this.setStatus(createInventoryEvent.getStatus());
        this.setCreateAt(createInventoryEvent.getCreateAt());
        this.setUpdateAt(createInventoryEvent.getUpdateAt());
        this.setDeleteAt(createInventoryEvent.getDeleteAt());
        this.setUserCreate(createInventoryEvent.getUserCreate());
        this.setUserUpdate(createInventoryEvent.getUserUpdate());
        this.setUserDelete(createInventoryEvent.getUserDelete());
    }

    @EventSourcingHandler
    public void on(UpdateInventoryEvent updateInventoryEvent) {
        log.info("updateInventoryEvent occurred.");
        this.inventoryId = updateInventoryEvent.getInventoryId();
        this.setProductId(updateInventoryEvent.getProductId());
        this.setStock(updateInventoryEvent.getStock());
        this.setUpdateAt(updateInventoryEvent.getUpdateAt());
        this.setUserUpdate(updateInventoryEvent.getUserUpdate());
    }

    @EventSourcingHandler
    public void on(DeleteInventoryEvent deleteInventoryEvent) {
        log.info("deleteInventoryEvent occurred.");
        this.inventoryId = deleteInventoryEvent.getInventoryId();
        this.setStatus(deleteInventoryEvent.getStatus());
        this.setDeleteAt(deleteInventoryEvent.getDeleteAt());
        this.setUserDelete(deleteInventoryEvent.getUserDelete());
    }

}
