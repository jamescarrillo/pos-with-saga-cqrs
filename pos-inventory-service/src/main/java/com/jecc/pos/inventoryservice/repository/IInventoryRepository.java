package com.jecc.pos.inventoryservice.repository;

import com.jecc.pos.entities.InventoryEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IInventoryRepository extends JpaRepository<InventoryEntity, String> {

    Optional<InventoryEntity> findByInventoryIdAndStatus(String inventoryId, String status);

    @Query(
            value = "select c from InventoryEntity c " +
                    "where c.status = :status " +
                    "and c.productId in (:productsId) "
    )
    Optional<List<InventoryEntity>> findEntities(
            @Param("status") String status,
            @Param("productsId") List<String> productsId);


    @Query(
            value = "select c from InventoryEntity c " +
                    "where c.status = :status and (c.productId = :productId) ")
    Optional<InventoryEntity> findByProductId(@Param("productId") String productId, @Param("status") String status);


    @Query(
            value = "select c from InventoryEntity c " +
                    "where c.status = :status and (c.productId = :productId) and c.inventoryId <> :inventoryId")
    Optional<InventoryEntity> findByProductId(@Param("productId") String productId, @Param("inventoryId") String inventoryId, @Param("status") String status);

}
