package com.jecc.pos.inventoryservice.controller;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.commands.inventory.create.CreateInventoryCommand;
import com.jecc.pos.commons.commands.inventory.delete.DeleteInventoryCommand;
import com.jecc.pos.commons.commands.inventory.update.UpdateInventoryCommand;
import com.jecc.pos.util.api.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("inventory")
@Log4j2
public class InventoryCommandRESTController {

    @Resource
    private Pipeline pipeline;

    @PostMapping
    public ResponseEntity<ApiResponse<CreateInventoryCommand>> create(@RequestBody CreateInventoryCommand createInventoryCommand) {
        return new ResponseEntity<>(this.pipeline.send(createInventoryCommand), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ApiResponse<UpdateInventoryCommand>> update(@RequestBody UpdateInventoryCommand updateInventoryCommand) {
        return new ResponseEntity<>(this.pipeline.send(updateInventoryCommand), HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<ApiResponse<DeleteInventoryCommand>> delete(@RequestBody DeleteInventoryCommand deleteInventoryCommand) {
        return new ResponseEntity<>(this.pipeline.send(deleteInventoryCommand), HttpStatus.CREATED);
    }


}
