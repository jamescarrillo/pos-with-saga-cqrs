package com.jecc.pos.inventoryservice.event;

import an.awesome.pipelinr.Pipeline;
import com.jecc.pos.commons.commands.inventory.create.CreateInventoryCommand;
import com.jecc.pos.commons.events.product.CreateProductEvent;
import com.jecc.pos.util.others.Event;
import com.jecc.pos.util.others.MapperUtil;
import lombok.extern.log4j.Log4j2;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Log4j2
public class ProductEventConsumer {

    @Resource
    private Pipeline pipeline;

    @KafkaListener(
            topics = "${topic.customer.name:products}",
            containerFactory = "kafkaListenerContainerFactory",
            groupId = "grupo1")
    public void consumer(Event<?> event) {
        log.info("Received event ... with Id {}, data {}",
                event.getId(),
                event.getData());
        CreateProductEvent createProductEvent = MapperUtil.deserialize(event.getData(), CreateProductEvent.class);
        log.info(createProductEvent);
        CreateInventoryCommand createInventoryCommand = new CreateInventoryCommand();
        createInventoryCommand.setProductId(createProductEvent.getProductId());
        createInventoryCommand.setStock(createProductEvent.getStock());
        createInventoryCommand.setUserCreate("JAMES CARRILLO");
        log.info("createInventoryCommand {}", createInventoryCommand.getProductId());
        log.info("createInventoryCommand {}", createInventoryCommand.getStock());
        try {
            pipeline.send(createInventoryCommand);
        }catch (Exception e){
            throw e;
        }
    }
}
