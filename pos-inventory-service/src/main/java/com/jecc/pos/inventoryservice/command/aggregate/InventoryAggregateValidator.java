package com.jecc.pos.inventoryservice.command.aggregate;

import com.jecc.pos.inventoryservice.repository.IInventoryRepository;
import com.jecc.pos.commons.commands.inventory.create.CreateInventoryCommand;
import com.jecc.pos.commons.commands.inventory.delete.DeleteInventoryCommand;
import com.jecc.pos.commons.commands.inventory.update.UpdateInventoryCommand;
import com.jecc.pos.entities.InventoryEntity;
import com.jecc.pos.util.constants.ConstantsLibrary;
import com.jecc.pos.util.constants.ErrorsConstants;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class InventoryAggregateValidator {

    private final IInventoryRepository inventoryRepository;

    @Autowired
    public InventoryAggregateValidator(IInventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    public void create(CreateInventoryCommand createInventoryCommand) {
        log.info("Validator createInventoryCommand init");
        Optional<InventoryEntity> optionalInventoryEntity =
                this.inventoryRepository.findByProductId(
                        createInventoryCommand.getProductId(),
                        ConstantsLibrary.STATUS_CREATED);
        if (optionalInventoryEntity.isPresent()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_ALREADY_EXIST_CODE, null, new String[]{ErrorsConstants.PRODUCT_ALREADY_EXIST_MESSAGE});
        }
        log.info("Validator createInventoryCommand fin");
    }

    public void update(UpdateInventoryCommand updateInventoryCommand) {
        log.info("Validator updateInventoryCommand init");
        //validation exist
        Optional<InventoryEntity> optionalInventoryEntityValidateExist = this.inventoryRepository.findByInventoryIdAndStatus(updateInventoryCommand.getInventoryId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalInventoryEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.PRODUCT_NOT_FOUND_MESSAGE});
        }
        Optional<InventoryEntity> optionalInventoryEntityValidate =
                this.inventoryRepository.findByProductId(
                        updateInventoryCommand.getProductId(),
                        updateInventoryCommand.getInventoryId(),
                        ConstantsLibrary.STATUS_CREATED);
        if (optionalInventoryEntityValidate.isPresent()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_ALREADY_EXIST_CODE, null, new String[]{ErrorsConstants.PRODUCT_ALREADY_EXIST_MESSAGE});
        }
        log.info("Validator updateInventoryCommand fin");
    }

    public void delete(DeleteInventoryCommand deleteInventoryCommand) {
        log.info("Validator deleteInventoryCommand init");
        //validation exist
        Optional<InventoryEntity> optionalInventoryEntityValidateExist = this.inventoryRepository.findByInventoryIdAndStatus(deleteInventoryCommand.getInventoryId(), ConstantsLibrary.STATUS_CREATED);
        if (optionalInventoryEntityValidateExist.isEmpty()) {
            throw new CommandExecutionException(ErrorsConstants.PRODUCT_NOT_FOUND_CODE, null, new String[]{ErrorsConstants.PRODUCT_NOT_FOUND_MESSAGE});
        }
        log.info("Validator deleteInventoryCommand fin");
    }

}
