﻿$currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
$testadmin = $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
if ($testadmin -eq $false) {
Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
exit $LASTEXITCODE
}

[System.Environment]::SetEnvironmentVariable("POS_POSTGRES_HOST_AND_PORT","localhost:5432","Machine");
[System.Environment]::SetEnvironmentVariable("POS_POSTGRES_DB","pos","Machine");
[System.Environment]::SetEnvironmentVariable("POS_POSTGRES_USERNAME","admin","Machine");
[System.Environment]::SetEnvironmentVariable("POS_POSTGRES_PASSWORD","test123","Machine");

[System.Environment]::SetEnvironmentVariable("POS_AXON_HOST_AND_PORT","localhost:8124","Machine");

[System.Environment]::SetEnvironmentVariable("POS_MONGO_HOST","localhost","Machine");
[System.Environment]::SetEnvironmentVariable("POS_MONGO_PORT","27017","Machine");
[System.Environment]::SetEnvironmentVariable("POS_MONGO_DB","pos","Machine");
[System.Environment]::SetEnvironmentVariable("POS_MONGO_USERNAME","root","Machine");
[System.Environment]::SetEnvironmentVariable("POS_MONGO_PASSWORD","root","Machine");

[System.Environment]::SetEnvironmentVariable("POS_KAFKA_HOST","localhost","Machine");
[System.Environment]::SetEnvironmentVariable("POS_KAFKA_PORT","9092","Machine");

[System.Environment]::SetEnvironmentVariable("POS_REDIS_TTL","5","Machine");
[System.Environment]::SetEnvironmentVariable("POS_REDIS_HOST","localhost","Machine");
[System.Environment]::SetEnvironmentVariable("POS_REDIS_PASSWORD","","Machine");
[System.Environment]::SetEnvironmentVariable("POS_REDIS_PORT","6379","Machine");
[System.Environment]::SetEnvironmentVariable("POS_REDIS_USERNAME","","Machine");

echo("Environments upgraded.");
pause
